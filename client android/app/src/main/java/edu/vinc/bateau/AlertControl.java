package edu.vinc.bateau;

import android.os.Handler;
import android.view.View;

public class AlertControl implements Runnable {
    private final MainActivity mActivity;
    private final View mView;
    private final Handler mHandler;
    private boolean mRunning;

    public AlertControl(MainActivity activity, View view) {
        this.mActivity = activity;
        this.mView = view;
        this.mHandler = new Handler();
        this.mRunning = false;
    }

    public void start() {
        // Check not already running
        if (!this.mRunning) {

            // Set running flag
            this.mRunning = true;

            // Force control invisible
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    mView.setVisibility(View.INVISIBLE);
                }
            });

            // Start the alert
            this.mHandler.post(this);
        }
    }

    public void stop(final int visibility) {
        // Reset running flag
        this.mRunning = false;

        // Remove the callbacks
        this.mHandler.removeCallbacks(this);

        // Set control visibility
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                mView.setVisibility(visibility);
            }
        });
    }

    @Override
    public void run() {
        // Check if alert is still running
        if (this.mRunning) {

            // Update control visibility
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    if (mView.getVisibility() == View.VISIBLE) {
                        // Set control invisible
                        mView.setVisibility(View.INVISIBLE);

                    } else {
                        // Set control visible
                        mView.setVisibility(View.VISIBLE);
                    }
                }
            });

            // Restart the alert in client-alert-refresh-rate ms
            this.mHandler.postDelayed(this, Integer.parseInt(this.mActivity.mSharedPrefs.getString("client-alert-refresh-rate", "500")));
        }
    }
}
