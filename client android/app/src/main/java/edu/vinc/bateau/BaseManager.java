package edu.vinc.bateau;

public abstract class BaseManager implements Runnable {

    final MainActivity mActivity;
    final BoatManager mBoatManager;

    final String LOG_TAG;

    public BaseManager(BoatManager manager) {
        // Save manager and activity
        this.mBoatManager = manager;
        this.mActivity = manager.mActivity;

        // Create log tag
        this.LOG_TAG = this.getClass().getName();
    }

    @Override
    public void run() {
        // Check can run
        if (this.mBoatManager.canRun()) {

            // Check can execute
            if (this.mBoatManager.canExecute()) {

                // Create a thread to run execute function
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        execute();
                    }
                });

                // Start the thread
                thread.start();

                // Stop all running managers in case of duplicate post
                this.stop(false);

                // Restart the manager in getDelay ms
                this.mBoatManager.mHandlerManager.postDelayed(this, this.getDelay());

            } else {

                // Stop all running managers in case of duplicate post
                this.stop(false);

                // Restart the manager in 1 second to handle settings close
                this.start(1000);
            }
        }
    }

    public void start(final long delay) {
        // Start the manager now
        this.mBoatManager.mHandlerManager.postDelayed(this, delay);
    }

    public void stop(final boolean forceStop) {
        // Remove all manager callbacks
        this.mBoatManager.mHandlerManager.removeCallbacks(this);
    }

    public void restart(final long delay) {
        // Stop all running manager
        this.stop(false);

        // Start the manager now
        this.start(delay);
    }

    protected abstract void execute();

    protected abstract long getDelay();

    protected abstract void resetConnection(final boolean updateState);
}
