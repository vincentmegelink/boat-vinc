package edu.vinc.bateau;

import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.webkit.WebViewClient;

// Boat manager class that start other management classes
public class BoatManager {

    public enum ServerType {
        FLASK, MOTION_CONFIG, MOTION_STREAM
    }

    public final MainActivity mActivity;
    public final Handler mHandlerManager;
    private final ConnectionManager mConnectionManager;
    private final CommandManager mCommandManager;
    private final StatusManager mStatusManager;
    private final CameraManager mCameraManager;
    private boolean mRunning;

    // private static final String LOG_TAG = BoatManager.class.getName();

    public BoatManager(MainActivity activity) {
        this.mActivity = activity;
        this.mHandlerManager = new Handler();
        this.mConnectionManager = new ConnectionManager(this);
        this.mCommandManager = new CommandManager(this);
        this.mStatusManager = new StatusManager(this);
        this.mCameraManager = new CameraManager(this);
        this.mRunning = false;
    }

    public void start() {
        // Check boat manager already running
        if (!this.mRunning) {

            // Set boat manager running
            this.mRunning = true;

            // Register for network scan results available
            this.mActivity.registerReceiver(this.mConnectionManager.getWifiBroadcastReceiver(), new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

            // Start all managers
            this.mCameraManager.start(0);
            this.mCommandManager.start(0);
            this.mStatusManager.start(0);
            this.mConnectionManager.start(0);
        }
    }

    public void stop(final boolean forceStop) {
        // Reset boat manager running
        this.mRunning = false;

        // Check force stop
        if (forceStop) {
            // Unregister network scan results available
            try {
                this.mActivity.unregisterReceiver(this.mConnectionManager.getWifiBroadcastReceiver());
            } catch (Exception ignored) {
            }

            // Stop all managers
            this.mCameraManager.stop(true);
            this.mCommandManager.stop(true);
            this.mStatusManager.stop(true);
            this.mConnectionManager.stop(true);
        }
    }

    public boolean isReadyForStatus() {
        return this.mConnectionManager.isReadyForStatus();
    }

    public boolean isReadyForCommand() {
        return this.mConnectionManager.isReadyForCommand();
    }

    public boolean isReadyForCamera() {
        return this.mConnectionManager.isReadyForCamera();
    }

    public boolean canRun() {
        return MainActivity.getApplicationRunning();
    }

    public boolean canExecute() {
        return this.mRunning && !MainActivity.getSettingsOpen() && MainActivity.getPermissionsGranted();
    }

    public WebViewClient getCameraWebViewClient() {
        return this.mCameraManager.getCameraWebViewClient();
    }

    public String getBoatIp(final ServerType boatServer) {
        return this.mConnectionManager.getBoatIp(boatServer);
    }

    public int getBoatWifiLevel() {
        return this.mConnectionManager.getBoatWifiLevel();
    }

    public void setFlaskServerError() {
        this.mConnectionManager.setFlaskServerError();
    }

    public void setFlaskServerStatusOk(boolean status) {
        this.mConnectionManager.setFlaskServerStatusOk(status);
    }

    public String sendRequestToBoatServer(final String urlString, final ServerType serverType) {
        return this.mConnectionManager.sendRequestToBoatServer(urlString, serverType);
    }

    public boolean sendSettingToBoatServer(final String key, final String value) {
        return this.mConnectionManager.sendSettingToBoatServer(key, value);
    }

    public void updateCameraState() {
        this.mCameraManager.updateCameraState();
    }

    public void restartCameraManager(final long delay) {
        // Restart the camera manager
        this.mCameraManager.restart(delay);
    }

    public void restartAllManagers(final long delay) {
        // Restart all managers
        this.mCameraManager.restart(delay);
        this.mCommandManager.restart(delay);
        this.mStatusManager.restart(delay);
        this.mConnectionManager.restart(delay);
    }

    public void resetConnection(final boolean updateState) {
        // Restart all managers
        this.mCameraManager.resetConnection(updateState);
        this.mCommandManager.resetConnection(updateState);
        this.mStatusManager.resetConnection(updateState);
        this.mConnectionManager.resetConnection(updateState);
    }
}
