package edu.vinc.bateau;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;

import static java.time.LocalDateTime.now;

public class CameraManager extends BaseManager {

    private Boolean mServerStarted;
    private Boolean mServerStarting;
    private Boolean mServerConfigured;
    private Boolean mServerConfiguring;
    private Boolean mStreamLoaded;
    private Boolean mStreamLoading;
    private Integer mStreamLoadingCounter;
    private Boolean mStreamLoadingError;
    private Instant mServerStartTime;
    private final CameraWebViewClient mCameraWebViewClient;

    public CameraManager(BoatManager manager) {
        super(manager);

        this.mCameraWebViewClient = new CameraWebViewClient();
        this.resetConnection(false);
    }

    @Override
    protected void execute() {
        // Check boat ready for camera streaming
        if (this.mBoatManager.isReadyForCamera()) {

            // Check camera activated
            if (this.mActivity.mSharedPrefs.getBoolean("camera-activated", false)) {

                // Check boat camera stream loading error
                if (!this.mStreamLoadingError) {

                    // Check boat camera server not started
                    if (!this.mServerStarted) {

                        // Start the boat camera stream
                        this.startBoatCameraServer();

                        // Check boat camera server starting
                        if (this.mServerStarting) {

                            // Check time elapsed since boat camera server started
                            if (this.checkServerStartupTime()) {
                                // Reset boat camera server starting
                                this.mServerStarting = false;

                                // Set camera server started
                                this.mServerStarted = true;

                                // Update camera state
                                this.updateCameraState();

                                // Restart the manager to start the stream loading
                                this.restart(0);
                            }
                        }

                    } else {
                        // Check boat camera server not configured
                        if (!this.mServerConfigured) {
                            // Send settings to the boat camera server
                            this.sendAllSettingsToCameraServer();

                        } else {
                            // Check boat camera stream not loaded
                            if (!this.mStreamLoaded) {
                                // Start the boat camera stream
                                this.startBoatCameraStream();
                            }
                        }
                    }

                } else {
                    // Update camera state
                    this.updateCameraState();
                }

            } else {
                // Check boat camera stream loaded
                if (this.mStreamLoaded) {
                    // Stop the boat camera stream
                    this.stopBoatCameraStream(true);

                } else {
                    // Check boat camera server started
                    if (this.mServerStarted) {
                        // Stop the boat camera server
                        this.stopBoatCameraServer();
                    }
                }
            }
        }
    }

    private boolean checkServerStartupTime() {
        // Check this.mServerStartTime not null
        if (this.mServerStartTime != null) {
            return Duration.between(this.mServerStartTime, Instant.now()).toMillis() >= Integer.parseInt(this.mActivity.mSharedPrefs.getString("client-boat-motion-stream-startup-time", "5000"));
        }

        return false;
    }

    @Override
    public void stop(final boolean forceStop) {
        super.stop(forceStop);

        // Check force stop
        if (forceStop) {
            // Check if the boat camera stream is loaded
            if (this.mStreamLoaded) {
                // Stop the boat camera stream
                this.stopBoatCameraStream(false);
            }

            // Check boat camera server started
            if (this.mServerStarted) {
                // Stop the boat camera server
                this.stopBoatCameraServer();
            }
        }
    }


    @Override
    protected long getDelay() {
        // Check if starting camera server
        if (this.mServerStarting) {

            // Set delay of 1 second if starting camera server
            return 1000;

        } else {

            // Return default delay from preferences
            return Integer.parseInt(this.mActivity.mSharedPrefs.getString("client-connect-retry-rate", "5000"));
        }
    }

    @Override
    public void resetConnection(final boolean updateState) {
        Log.e(LOG_TAG, now().toString() + " - resetConnection");

        // Reset camera server started
        this.mServerStarted = false;

        // Reset camera server starting
        this.mServerStarting = false;

        // Reset camera server configured
        this.mServerConfigured = false;

        // Reset camera server configuring
        this.mServerConfiguring = false;

        // Reset camera server started time
        this.mServerStartTime = null;

        // Reset camera stream loaded
        this.mStreamLoaded = false;

        // Reset camera stream loading
        this.mStreamLoading = false;

        // Reset camera stream loading counter
        this.mStreamLoadingCounter = 0;

        // Reset camera stream loading error
        this.mStreamLoadingError = false;

        // Update the camera state if asked
        if (updateState) {
            this.updateCameraState();
        }
    }

    private void startBoatCameraServer() {
        // Check camera server not already starting
        if (!this.mServerStarting) {
            Log.e(LOG_TAG, now().toString() + " - startBoatCameraServer");

            // Set camera server starting flag
            this.mServerStarting = true;

            // Update camera state
            this.updateCameraState();

            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(mActivity.getApplicationContext(), "Starting the boat camera server...", Toast.LENGTH_SHORT).show();
                }
            });

            // Send start camera request to the boat server
            String response = this.mBoatManager.sendRequestToBoatServer("/boat/start/camera", BoatManager.ServerType.FLASK);

            // Check response
            if (response.startsWith("OK")) {
                // Set camera server started time
                this.mServerStartTime = Instant.now();

                // Reset camera stream loading counter
                this.mStreamLoadingCounter = 0;

            } else {
                this.mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(mActivity.getApplicationContext(), "Enable to start the boat camera...", Toast.LENGTH_SHORT).show();
                    }
                });

                // Reset camera server starting
                this.mServerStarting = false;
            }

            // Update camera state
            this.updateCameraState();
        }
    }

    private void stopBoatCameraServer() {
        Log.e(LOG_TAG, now().toString() + " - stopBoatCameraServer");

        // Send stop camera request to the boat server
        String response = this.mBoatManager.sendRequestToBoatServer("/boat/stop/camera", BoatManager.ServerType.FLASK);

        // Check response
        if (!response.startsWith("OK")) {
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(mActivity.getApplicationContext(), "Enable to stop the boat camera...", Toast.LENGTH_SHORT).show();
                }
            });
        }

        // Reset camera connection
        this.resetConnection(true);
    }

    private void startBoatCameraStream() {
        // Check camera stream not already loading
        if (!this.mStreamLoading) {
            Log.e(LOG_TAG, now().toString() + " - startBoatCameraStream");

            // Set camera stream loading flag
            this.mStreamLoading = true;

            // Update camera state
            this.updateCameraState();

            // Start loading the stream and show the camera web view
            this.mActivity.runOnUiThread(new Runnable() {
                @SuppressLint("SetJavaScriptEnabled")
                public void run() {
                    // Load the stream from boat server port 8081 (motion need to run as daemon on the RPi)
                    String streamUrl = "http://" + mBoatManager.getBoatIp(BoatManager.ServerType.MOTION_STREAM);
                    mActivity.mWebViewCamera.loadUrl(streamUrl);

                    // Make the camera web view visible
                    mActivity.mWebViewCamera.setVisibility(View.VISIBLE);

                    // Increase camera stream loading counter
                    mStreamLoadingCounter += 1;
                }
            });
        }
    }

    private void stopBoatCameraStream(final boolean restartServer) {
        Log.e(LOG_TAG, now().toString() + " - stopBoatCameraStream");

        // Stop loading the stream and hide the camera web view
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                // Stop loading the boat camera stream
                mActivity.mWebViewCamera.stopLoading();

                // Make the camera web view invisible
                mActivity.mWebViewCamera.setVisibility(View.INVISIBLE);
            }
        });

        // Reset camera stream loaded
        this.mStreamLoaded = false;

        // Reset camera stream loading
        this.mStreamLoading = false;

        // Reset camera stream loading counter
        this.mStreamLoadingCounter = 0;

        // Reset camera stream loading error
        this.mStreamLoadingError = false;

        // Restart the manager
        if (restartServer) {
            this.restart(0);
        }
    }

    public WebViewClient getCameraWebViewClient() {
        return this.mCameraWebViewClient;
    }

    class CameraWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // Reset camera stream loading error
            mStreamLoadingError = false;

            // Reset camera stream loading flag
            mStreamLoading = false;

            // Set camera stream loaded flag
            mStreamLoaded = true;

            // Update camera state
            updateCameraState();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            // Check ready for camera
            if (mBoatManager.isReadyForCamera()) {

                // Check if the boat camera is activated in settings
                if (mActivity.mSharedPrefs.getBoolean("camera-activated", false)) {

                    // Check the stream loading counter
                    if (mStreamLoadingCounter < 5) {
                        // Reset camera stream loading flag
                        mStreamLoading = false;

                        // Reset camera stream loaded flag
                        mStreamLoaded = false;

                    } else {
                        // Stop the stream
                        stopBoatCameraStream(true);

                        // Set camera stream loading error
                        mStreamLoadingError = true;
                    }
                }
            }
        }
    }

    private void sendAllSettingsToCameraServer() {
        // Check not already sending settings
        if (!this.mServerConfiguring) {
            Log.e(LOG_TAG, now().toString() + " - sendAllSettingsToCameraServer");

            // Set boat flask server configuring flag
            this.mServerConfiguring = true;

            // Update the camera state
            this.updateCameraState();

            // Send all settings to the boat camera server
            boolean settingsSentOk = true;
            try {

                // Place all shared preferences in a map object
                Map<String, ?> keys = this.mActivity.mSharedPrefs.getAll();

                // Iterate all shared preferences
                for (Map.Entry<String, ?> entry : keys.entrySet()) {

                    // Send all server/motor/direction/sensors settings to the boat server
                    if (entry.getKey().startsWith("camera")) {
                        if (!this.mBoatManager.sendSettingToBoatServer(entry.getKey(), entry.getValue().toString())) {
                            Log.e(LOG_TAG, now().toString() + " - sendAllSettingsToCameraServer failed to send setting:" + entry.getKey());

                            // Reset settings sent ok flag
                            settingsSentOk = false;

                            break;
                        }
                    }
                }

                // Check settingsSentOk
                if (settingsSentOk) {

                    // Set boat flask server configured flag
                    this.mServerConfigured = true;

                    // Update the camera state
                    this.updateCameraState();
                }

            } catch (Exception ignored) {
                // Reset settings sent ok flag
                settingsSentOk = false;
            }

            // Reset boat camera server configuring flag
            this.mServerConfiguring = false;

            // Update the wifi state
            this.updateCameraState();

            // Check all settings sent Ok
            if (settingsSentOk) {
                this.mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(mActivity.getApplicationContext(),
                                "Boat camera server started",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                // Restart the manager after all settings sent ok
                this.restart(0);
            }
        }
    }

    public void updateCameraState() {
        // Check boat camera activated
        if (this.mActivity.mSharedPrefs.getBoolean("camera-activated", false)) {

            // Update camera button state
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    // Check boat camera stream loading error
                    if (mStreamLoadingError) {
                        // Update camera button icon
                        mActivity.mButtonCamera.setBackgroundResource(R.drawable.camera);

                        // Stop the camera state alert
                        mActivity.mAlertControlCameraState.stop(View.VISIBLE);

                    } else {
                        // Check boat camera server not started
                        if (!mServerStarted) {
                            // Update camera button icon
                            mActivity.mButtonCamera.setBackgroundResource(R.drawable.camera_red);

                            // Check boat camera server starting
                            if (mServerStarting) {
                                // Start the camera state alert
                                mActivity.mAlertControlCameraState.start();

                            } else {
                                // Stop the camera state alert
                                mActivity.mAlertControlCameraState.stop(View.VISIBLE);
                            }

                        } else {
                            // Check boat camera stream not loaded
                            if (!mStreamLoaded) {
                                // Update camera button icon
                                mActivity.mButtonCamera.setBackgroundResource(R.drawable.camera_green);

                                // Check boat camera stream loading
                                if (mStreamLoading) {
                                    // Start the camera state alert
                                    mActivity.mAlertControlCameraState.start();
                                }

                            } else {
                                // Stop the camera state alert
                                mActivity.mAlertControlCameraState.stop(View.VISIBLE);
                            }
                        }
                    }
                }
            });
        }
    }
}
