package edu.vinc.bateau;

public class CommandManager extends BaseManager {

    public CommandManager(BoatManager manager) {
        super(manager);
    }

    @Override
    protected void execute() {
        // Check boat ready for command sending
        if (this.mBoatManager.isReadyForCommand()) {

            // Get speed and direction from progress bars
            int speed = this.mActivity.mSeekBarSpeed.getProgress();
            int direction = this.mActivity.mSeekBarDirection.getProgress() + this.mActivity.mSeekBarTrimDirection.getProgress();

            // Check reverse direction
            if (this.mActivity.mSharedPrefs.getBoolean("client-reverse-direction", false)) {
                float middlePos = (Float.parseFloat(this.mActivity.mSharedPrefs.getString("direction-max-angle", "180.0")) -
                        Float.parseFloat(this.mActivity.mSharedPrefs.getString("direction-min-angle", "0.0"))) / 2;
                direction = Math.round(middlePos - (direction - middlePos));
            }

            // Check direction limits
            if (direction < Math.round(Float.parseFloat(this.mActivity.mSharedPrefs.getString("direction-min-angle", "0.0")))) {
                direction = Math.round(Float.parseFloat(this.mActivity.mSharedPrefs.getString("direction-min-angle", "0.0")));

            } else if (direction > Math.round(Float.parseFloat(this.mActivity.mSharedPrefs.getString("direction-max-angle", "180.0")))) {
                direction = Math.round(Float.parseFloat(this.mActivity.mSharedPrefs.getString("direction-max-angle", "180.0")));
            }

            // Send new speed and direction to the boat server
            String response = this.mBoatManager.sendRequestToBoatServer("/boat/move/" + speed + "/" + direction, BoatManager.ServerType.FLASK);

            // Check response
            if (!response.startsWith("OK")) {
                // Set boat flask server error
                this.mBoatManager.setFlaskServerError();
            }
        }
    }

    @Override
    public void stop(final boolean forceStop) {
        super.stop(forceStop);
    }

    @Override
    protected long getDelay() {
        return Integer.parseInt(this.mActivity.mSharedPrefs.getString("client-move-refresh-rate", "100"));
    }

    @Override
    public void resetConnection(final boolean updateState) { }
}
