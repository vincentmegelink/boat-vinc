package edu.vinc.bateau;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiNetworkSpecifier;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.net.InetAddress;
import java.util.List;
import java.util.Map;

import static java.time.LocalDateTime.now;

public class ConnectionManager extends BaseManager {

    private final WifiManager mWifiManager;
    private final WifiBroadcastReceiver mWifiReceiver;
    private final ConnectivityManager mConnectivityManager;
    private final WifiNetworkCallback mWifiNetworkCallback;
    private String mBoatIp;
    private boolean mWifiFound;
    private boolean mWifiConnected;
    private boolean mWifiConnecting;
    private int mWifiScanCount;
    private boolean mWifiScanError;
    private boolean mWifiScanning;
    private boolean mFlaskServerStarted;
    private boolean mFlaskServerStarting;
    private boolean mFlaskServerConfigured;
    private boolean mFlaskServerConfiguring;
    private boolean mFlaskServerStatusOk;
    private int mFlaskServerErrorCount;
    private boolean mWifiDisabledAlertDialogOpen;

    public ConnectionManager(BoatManager manager) {
        super(manager);

        this.mWifiManager = (WifiManager) this.mActivity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        this.mWifiReceiver = new WifiBroadcastReceiver();
        this.mConnectivityManager = (ConnectivityManager) this.mActivity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        this.mWifiNetworkCallback = new WifiNetworkCallback();
        this.resetConnection(false);
    }

    @Override
    protected void execute() {
        // Check permissions not granted
        if (!MainActivity.getPermissionsGranted()) {
            // Request permissions
            this.mActivity.requestBoatPermissions();

        } else {
            // Check wifi not enabled
            if (!this.mWifiManager.isWifiEnabled()) {

                // Check wifi disabled alert dialog not already open
                if (!this.mWifiDisabledAlertDialogOpen) {

                    // Set wifi disabled alert dialog open
                    this.mWifiDisabledAlertDialogOpen = true;

                    // Open a dialog to ask to enable wifi (Android Q don't allow to enable wifi in app)
                    this.mActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            new AlertDialog.Builder(mActivity)
                                    .setTitle("Warning")
                                    .setMessage("Please turn on wifi")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Open the wifi settings
                                            Intent myIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                            mActivity.startActivity(myIntent);

                                            // Reset wifi disabled alert dialog open
                                            mWifiDisabledAlertDialogOpen = false;
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Reset wifi disabled alert dialog open
                                            mWifiDisabledAlertDialogOpen = false;

                                            // Restart the connection manager to ask again...
                                            restart(0);
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    });
                }

            } else {
                // Check boat wifi hotspot found
                if (!this.mWifiFound) {
                    // Search for boat wifi hotspot
                    this.searchBoatWifi();

                } else {

                    // Check boat wifi connection flag
                    if (!this.mWifiConnected) {
                        // Search for boat wifi hotspot
                        this.connectBoatWifi();

                    } else {
                        // Check boat flask server not started or in error
                        if (!this.mFlaskServerStarted || this.isFlaskServerInError()) {
                            // Start the boat flask server
                            this.startBoatServer();

                        } else {
                            // Check boat flask server not configured
                            if (!this.mFlaskServerConfigured) {
                                // Send all settings to the boat flask server
                                this.sendAllSettingsToBoatServer();
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void stop(final boolean forceStop) {
        super.stop(forceStop);

        // Check force stop
        if (forceStop) {
            // Stop the boat flask server
            this.stopBoatServer(false);

            // Disconnect from the boat wifi hotspot
            this.disconnectBoatWifi();
        }
    }

    @Override
    protected long getDelay() {
        // Default delay to client-connect-retry-rate ms
        long delay = Integer.parseInt(this.mActivity.mSharedPrefs.getString("client-connect-retry-rate", "5000"));

        // Check wifi enabled and boat wifi hotspot not found
        if (this.mWifiManager.isWifiEnabled() && !this.mWifiFound) {

            // Check wifi scan counter (4 scans allowed in 120 seconds)
            if (this.mWifiScanCount >= 3) {
                // Set delay to 80 seconds to complete the 120 seconds restriction
                delay = 80000;

                // Reset wifi scan counter
                this.mWifiScanCount = 0;

                // Set wifi scan count error flag
                this.mWifiScanError = true;

            } else {
                // Set delay to 10 seconds between wifi scan
                delay = 10000;
            }
        }

        return delay;
    }

    @Override
    public void resetConnection(final boolean updateState) {
        Log.e(LOG_TAG, now().toString() + " - resetConnection");

        // Reset boat ip address
        this.mBoatIp = "";

        // Reset boat wifi found flag
        this.mWifiFound = false;

        // Reset boat wifi connected flag
        this.mWifiConnected = false;

        // Reset boat wifi connecting flag
        this.mWifiConnecting = false;

        // Reset wifi scan counter
        this.mWifiScanCount = 0;

        // Reset wifi scan count error flag
        this.mWifiScanError = false;

        // Reset wifi scanning flag
        this.mWifiScanning = false;

        // Reset boat flask server started flag
        this.mFlaskServerStarted = false;

        // Reset boat flask server starting flag
        this.mFlaskServerStarting = false;

        // Reset boat flask server configured flag
        this.mFlaskServerConfigured = false;

        // Reset boat flask server configuring flag
        this.mFlaskServerConfiguring = false;

        // Reset boat flask server error counter
        this.mFlaskServerErrorCount = 0;

        // Reset boat flask server status ok
        this.mFlaskServerStatusOk = false;

        // Unbind process to boat wifi network
        this.mConnectivityManager.bindProcessToNetwork(null);

        // Update the wifi state if asked
        if (updateState) {
            this.updateWifiState();
        }
    }

    public boolean isReadyForStatus() {
        return this.isWifiReady()
                && this.isFlaskServerReady()
                && this.isFlaskServerConfigured()
                && !this.isFlaskServerInError();
    }

    public boolean isReadyForCommand() {
        return this.isReadyForStatus() && this.mFlaskServerStatusOk;
    }

    public boolean isReadyForCamera() {
        return this.isWifiReady()
                && this.isFlaskServerReady()
                && !this.isFlaskServerInError();
    }

    private boolean isWifiReady() {
        return this.mWifiFound && this.mWifiConnected;
    }

    private boolean isFlaskServerReady() {
        return this.mFlaskServerStarted && !this.mFlaskServerStarting;
    }

    private boolean isFlaskServerConfigured() {
        return this.mFlaskServerConfigured && !this.mFlaskServerConfiguring;
    }

    private boolean isFlaskServerInError() {
        return this.mFlaskServerErrorCount >= Integer.parseInt(this.mActivity.mSharedPrefs.getString("client-boat-flask-server-max-error", "5"));
    }

    public void setFlaskServerError() {
        this.mFlaskServerErrorCount++;
        Log.e(LOG_TAG, now().toString() + " - setFlaskServerError: " + this.mFlaskServerErrorCount);

        // Check boat flask server in error
        if (this.isFlaskServerInError()) {
            // Restart the boat server to try to recover the error
            this.startBoatServer();
        }
    }

    public void setFlaskServerStatusOk(boolean status) {
        this.mFlaskServerStatusOk = status;
        Log.e(LOG_TAG, now().toString() + " - setFlaskServerStatusOk: " + this.mFlaskServerStatusOk);
    }

    public WifiBroadcastReceiver getWifiBroadcastReceiver() {
        return this.mWifiReceiver;
    }

    public String getBoatIp(final BoatManager.ServerType boatServer) {
        // Check boat server ip not empty
        if (!this.mBoatIp.isEmpty()) {

            // Switch boat server
            switch (boatServer) {
                case FLASK: {
                    return this.mBoatIp + ":" + this.mActivity.mSharedPrefs.getString("client-boat-flask-server-port", "5000");
                }

                case MOTION_CONFIG: {
                    return this.mBoatIp + ":" + this.mActivity.mSharedPrefs.getString("client-boat-motion-config-server-port", "8080");
                }

                case MOTION_STREAM: {
                    return this.mBoatIp + ":" + this.mActivity.mSharedPrefs.getString("client-boat-motion-stream-server-port", "8081");
                }
            }
        }

        return "";
    }

    public int getBoatWifiLevel() {
        int wifiLevel = 0;

        // Check wifi ready
        if (this.isWifiReady()) {

            // Get wifi signal level
            WifiInfo wifiInfo = this.mWifiManager.getConnectionInfo();
            wifiLevel = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), 100);
        }

        return wifiLevel;
    }

    @SuppressWarnings("deprecation")
    private void searchBoatWifi() {
        // Check not already scanning
        if (!this.mWifiScanning) {
            Log.e(LOG_TAG, now().toString() + " - searchBoatWifi");

            // Set wifi scanning flag
            this.mWifiScanning = true;

            // Searching for boat wifi hotspot
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(mActivity.getApplicationContext(),
                            "Searching boat wifi hotspot...",
                            Toast.LENGTH_LONG).show();
                }
            });

            // Update the wifi state
            this.updateWifiState();

            //Check for permissions
            if (MainActivity.getPermissionsGranted()) {
                // Increment wifi scan counter
                this.mWifiScanCount++;

                // Start scanning for wifi hotspot (Limited to 4 scans per 2 minutes)
                this.mWifiManager.startScan();

            } else {
                //Request permissions
                ActivityCompat.requestPermissions(this.mActivity,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_WIFI_STATE,
                                Manifest.permission.CHANGE_WIFI_STATE,
                                Manifest.permission.ACCESS_NETWORK_STATE,
                                Manifest.permission.CHANGE_NETWORK_STATE},
                        MainActivity.BOAT_PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    class WifiBroadcastReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            // Check boat wifi not already found
            if (!mWifiFound) {
                Log.e(LOG_TAG, now().toString() + " - WifiBroadcastReceiver.onReceive");

                // Check scan result
                boolean result = intent.getBooleanExtra(WifiManager.EXTRA_RESULTS_UPDATED, false);
                if (result) {
                    // Get list of found networks
                    List<ScanResult> list = mWifiManager.getScanResults();

                    // Loop all found networks
                    for (ScanResult scanResult : list) {

                        // Check if boat wifi hotspot found
                        if (scanResult.SSID.equals(mActivity.mSharedPrefs.getString("client-boat-wifi-hotspot", "Boat-Wifi"))) {
                            Log.e(LOG_TAG, now().toString() + " - WifiBroadcastReceiver.onReceive Boat-Wifi found");

                            // Set boat wifi hotspot found
                            mWifiFound = true;

                            // Reset wifi scanning flag
                            mWifiScanning = false;

                            // Update the wifi state
                            updateWifiState();

                            // Restart the connection manager to connect to the boat wifi
                            restart(0);

                            return;
                        }
                    }

                    // Check wifi scan error
                    if (mWifiScanError) {
                        Log.e(LOG_TAG, now().toString() + " - WifiBroadcastReceiver.onReceive 4 scans without finding Boat-Wifi !!!");

                        // Boat wifi hotspot not found after 4 scans
                        mActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(mActivity.getApplicationContext(),
                                        "Boat wifi hotspot not found after 4 scans...\nWait 80 seconds or close the app and retry",
                                        Toast.LENGTH_LONG).show();
                            }
                        });

                        // Reset wifi scan count error
                        mWifiScanError = false;

                    } else {
                        Log.e(LOG_TAG, now().toString() + " - WifiBroadcastReceiver.onReceive Boat-Wifi not found...");

                        // Boat wifi hotspot not found
                        mActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(mActivity.getApplicationContext(),
                                        "Boat wifi hotspot not found...",
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }

                // Reset wifi scanning flag
                mWifiScanning = false;

                // Update the wifi state
                updateWifiState();
            }
        }
    }

    private void connectBoatWifi() {
        // Check wifi not already connecting
        if (!this.mWifiConnecting) {
            Log.e(LOG_TAG, now().toString() + " - connectBoatWifi");

            // Set wifi connecting flag
            this.mWifiConnecting = true;

            // Update the wifi state
            this.updateWifiState();

            // Create the network specifier to connect the boat wifi hotspot
            WifiNetworkSpecifier wifiNetworkSpecifier = new WifiNetworkSpecifier.Builder()
                    .setSsid(mActivity.mSharedPrefs.getString("client-boat-wifi-hotspot", "Boat-Wifi"))
                    .build();

            // Create the request to connect the boat wifi hotspot
            NetworkRequest networkRequest = new NetworkRequest.Builder()
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .removeCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .setNetworkSpecifier(wifiNetworkSpecifier)
                    .build();

            // Connect to the boat wifi hotspot
            this.mConnectivityManager.requestNetwork(networkRequest, this.mWifiNetworkCallback);
        }
    }

    private void disconnectBoatWifi() {
        Log.e(LOG_TAG, now().toString() + " - disconnectBoatWifi");

        try {
            // Disconnect from boat wifi hotspot
            this.mConnectivityManager.unregisterNetworkCallback(this.mWifiNetworkCallback);
        }catch (IllegalArgumentException ignored){
        }

        // Reset all connections
        this.mBoatManager.resetConnection(true);
    }

    class WifiNetworkCallback extends ConnectivityManager.NetworkCallback {

        @Override
        public void onAvailable(@NonNull Network network) {
            super.onAvailable(network);
            Log.e(LOG_TAG, now().toString() + " - WifiNetworkCallback.onAvailable");

            // Check boat ip address available in link properties
            LinkProperties linkProperties = mConnectivityManager.getLinkProperties(network);
            if (linkProperties != null) {

                // Get DNS server properties
                List<InetAddress> dnsServers = linkProperties.getDnsServers();
                if (dnsServers.size() >= 1) {
                    Log.e(LOG_TAG, now().toString() + " - WifiNetworkCallback.onAvailable Boat-Wifi connected");

                    // Save boat ip address from first DNS server
                    mBoatIp = dnsServers.get(0).getHostAddress();

                    // Bind process to boat wifi network
                    mConnectivityManager.bindProcessToNetwork(network);

                    // Set boat wifi connected flag
                    mWifiConnected = true;

                    // Restart the connection manager to start the boat flask server
                    restart(0);

                } else {
                    Log.e(LOG_TAG, now().toString() + " - WifiNetworkCallback.onAvailable Boat-Wifi IP not found in DNS !!!");

                    // Something is wrong with the wifi
                    mActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(mActivity.getApplicationContext(),
                                    "Something is wrong with the wifi...",
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                    // Reset all connections
                    mBoatManager.resetConnection(true);

                    // Restart the connection manager in 2 seconds to restart the connection process
                    restart(2000);
                }

            } else {
                Log.e(LOG_TAG, now().toString() + " - WifiNetworkCallback.onAvailable Boat-Wifi IP not found in link properties !!!");

                // Something is wrong with the wifi
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(mActivity.getApplicationContext(),
                                "Something is wrong with the wifi...",
                                Toast.LENGTH_LONG).show();
                    }
                });

                // Reset all connections
                mBoatManager.resetConnection(true);

                // Restart the connection manager in 2 seconds to restart the connection process
                restart(2000);
            }

            // Update the wifi state
            updateWifiState();
        }

        @Override
        public void onUnavailable() {
            super.onUnavailable();
            Log.e(LOG_TAG, now().toString() + " - WifiNetworkCallback.onUnavailable Boat-Wifi disconnected");

            // Reset all connections
            mBoatManager.resetConnection(true);
        }

        @Override
        public void onLost(@NonNull Network network) {
            super.onLost(network);
            Log.e(LOG_TAG, now().toString() + " - WifiNetworkCallback.onUnavailable Boat-Wifi lost");

            // Reset all connections
            mBoatManager.resetConnection(true);
        }
    }

    private void startBoatServer()  {
        // Check boat flask server not already starting
        if (!this.mFlaskServerStarting) {
            Log.e(LOG_TAG, now().toString() + " - startBoatServer");

            // Set boat flask server starting flag
            this.mFlaskServerStarting = true;

            // Update the wifi state
            this.updateWifiState();

            // Check boat flask server in error
            if (this.isFlaskServerInError()) {
                Log.e(LOG_TAG, now().toString() + " - startBoatServer recover after error");

                // Send status request to the boat server
                String response = this.sendRequestToBoatServer("/boat/status", BoatManager.ServerType.FLASK);

                // Check response
                if (!response.startsWith("OK")) {
                    Log.e(LOG_TAG, now().toString() + " - startBoatServer recover failed");

                    // Stop the boat server
                    this.stopBoatServer(true);

                } else {
                    Log.e(LOG_TAG, now().toString() + " - startBoatServer connection recovered");

                    // Reset boat server error counter
                    this.mFlaskServerErrorCount = 0;

                    // Set boat server started flag
                    this.mFlaskServerStarted = true;

                    // Reset boat flask server starting flag
                    this.mFlaskServerStarting = false;
                }
            }

            // Check boat server already started
            if (this.mFlaskServerStarted) {
                // Reset boat flask server starting flag
                this.mFlaskServerStarting = false;

                // Update the wifi state
                this.updateWifiState();

                return;
            }

            // Request start of python process from the boat server
            Log.e(LOG_TAG, now().toString() + " - startBoatServer starting boat server...");
            String response = this.sendRequestToBoatServer("/boat/start/all", BoatManager.ServerType.FLASK);
            if (!response.startsWith("OK")) {
                Log.e(LOG_TAG, now().toString() + " - startBoatServer failed to start boat server with response: " + response);

                this.mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(mActivity.getApplicationContext(),
                                "Enable to start the boat server...",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                // Reset boat flask server starting flag
                this.mFlaskServerStarting = false;

                // Update the wifi state
                this.updateWifiState();

                return;
            }
            Log.e(LOG_TAG, now().toString() + " - startBoatServer boat server started OK");


            // Reset boat flask server starting flag
            this.mFlaskServerStarting = false;

            // Set boat server started flag
            this.mFlaskServerStarted = true;

            // Update the wifi state
            this.updateWifiState();

            // Restart the connection manager to send the settings to the boat flask server
            this.restart(0);
        }
    }

    private void stopBoatServer(final boolean restartServer)  {
        Log.e(LOG_TAG, now().toString() + " - stopBoatServer");

        // Send stop request to the boat server
        this.sendRequestToBoatServer("/boat/stop/all", BoatManager.ServerType.FLASK);

        // Reset all connections
        this.mBoatManager.resetConnection(true);

        // Restart the connection manager to restart the boat server
        if (restartServer) {
            this.restart(this.getDelay());
        }
    }

    public boolean sendSettingToBoatServer(final String key, final String value) {
        Log.e(LOG_TAG, now().toString() + " - sendSettingToBoat key:" + key + " value:" + value);

        // Send all server/motor/direction/sensors/camera settings to the boat server
        String part = "";
        if (key.startsWith("server")) {
            part = "server";
        } else if (key.startsWith("motor")) {
            part = "motor";
        } else if (key.startsWith("direction")) {
            part = "direction";
        } else if (key.startsWith("sensors")) {
            part = "sensors";
        } else if (key.startsWith("camera") && !key.endsWith("activated") && this.mActivity.mSharedPrefs.getBoolean("camera-activated", false)) {
            part = "camera";
        }

        // Check if setting need to be send to the boat server
        if (!part.isEmpty()) {

            // Check if camera setting
            if (part.equals("camera")) {

                // Send camera setting to the boat motion web control server (Ex: http://192.168.1.79:8080/0/config/set?brightness=128)
                String keyUrl = key.replace("camera-", "").replace("-", "_");
                String response = this.sendRequestToBoatServer("/0/config/set?" + keyUrl + "=" + value, BoatManager.ServerType.MOTION_CONFIG);

                // Check response
                if (response.contains("Done")) {
                    Log.e(LOG_TAG, now().toString() + " - sendSettingToBoat OK");
                    return true;

                } else {
                    Log.e(LOG_TAG, now().toString() + " - sendSettingToBoat KO");

                    this.mActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(mActivity.getApplicationContext(), "Enable to send setting to the boat camera server...", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return false;
                }

            } else {
                // Send all other setting to the boat flask server
                String response = this.sendRequestToBoatServer("/boat/settings/" + part + "/" + key + "/" + value, BoatManager.ServerType.FLASK);

                // Check response
                if (response.startsWith("OK")) {
                    Log.e(LOG_TAG, now().toString() + " - sendSettingToBoat OK");
                    return true;

                } else {
                    Log.e(LOG_TAG, now().toString() + " - sendSettingToBoat KO");
                    this.mActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(mActivity.getApplicationContext(), "Enable to send setting to the boat flask server...", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return false;
                }
            }
        }

        Log.e(LOG_TAG, now().toString() + " - sendSettingToBoat Skipped preference:" + key);
        return true;
    }

    private void sendAllSettingsToBoatServer() {
        // Check not already sending settings
        if (!this.mFlaskServerConfiguring) {
            Log.e(LOG_TAG, now().toString() + " - sendAllSettingsToBoatServer");

            // Set boat flask server configuring flag
            this.mFlaskServerConfiguring = true;

            // Update the wifi state
            this.updateWifiState();

            // Send all settings to the boat flask server
            boolean settingsSentOk = true;
            try {

                // Place all shared preferences in a map object
                Map<String, ?> keys = this.mActivity.mSharedPrefs.getAll();

                // Iterate all shared preferences
                for (Map.Entry<String, ?> entry : keys.entrySet()) {

                    // Send all server/motor/direction/sensors settings to the boat server
                    if (entry.getKey().startsWith("server")
                        || entry.getKey().startsWith("motor")
                        || entry.getKey().startsWith("direction")
                        || entry.getKey().startsWith("sensors")) {
                        if (!this.sendSettingToBoatServer(entry.getKey(), entry.getValue().toString())) {
                            Log.e(LOG_TAG, now().toString() + " - sendAllSettingsToBoatServer failed to send setting:" + entry.getKey());

                            // Reset settings sent ok flag
                            settingsSentOk = false;

                            break;
                        }
                    }
                }

                // Check settingsSentOk
                if (settingsSentOk) {

                    // Set boat flask server configured flag
                    this.mFlaskServerConfigured = true;

                    // Update the wifi state
                    this.updateWifiState();
                }

            } catch (Exception ignored) {
                // Reset settings sent ok flag
                settingsSentOk = false;
            }

            // Reset boat flask server configuring flag
            this.mFlaskServerConfiguring = false;

            // Update the wifi state
            this.updateWifiState();

            // Check all settings sent Ok
            if (settingsSentOk) {
                this.mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(mActivity.getApplicationContext(),
                                "Boat server started",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                // Restart all managers after all settings sent ok
                this.mBoatManager.restartAllManagers(0);
            }
        }
    }

    public String sendRequestToBoatServer(String urlString, final BoatManager.ServerType serverType) {
        final String result = "KO";

        // Check boat ip not empty
        if (!this.mBoatIp.isEmpty()) {

            // Create a http requester
            HttpRequester httpRequester = new HttpRequester(this);

            // Send the request to the boat server using the http requester
            return httpRequester.sendRequest(urlString, serverType);
        }

        return result;
    }

    private void updateWifiState() {
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                // Check boat wifi hotspot not found
                if (!mWifiFound) {
                    // Update wifi button icon
                    mActivity.mButtonWifi.setBackgroundResource(R.drawable.wifi_red);

                    // Check boat wifi hotspot scanning
                    if (mWifiScanning) {
                        // Start the wifi state alert
                        mActivity.mAlertControlWifiState.start();

                    } else {
                        // Stop the wifi state alert
                        mActivity.mAlertControlWifiState.stop(View.VISIBLE);
                    }

                } else {
                    // Check boat wifi hotspot not connected
                    if (!mWifiConnected) {
                        // Update wifi button icon
                        mActivity.mButtonWifi.setBackgroundResource(R.drawable.wifi_orange);

                        // Check boat wifi hotspot connecting
                        if (mWifiConnecting) {
                            // Start the wifi state alert
                            mActivity.mAlertControlWifiState.start();

                        } else {
                            // Stop the wifi state alert
                            mActivity.mAlertControlWifiState.stop(View.VISIBLE);
                        }

                    } else {
                        // Check boat flask server not started
                        if (!mFlaskServerStarted) {
                            // Update wifi button icon
                            mActivity.mButtonWifi.setBackgroundResource(R.drawable.wifi_yellow);

                            // Check boat flask server starting
                            if (mFlaskServerStarting) {
                                // Start the wifi state alert
                                mActivity.mAlertControlWifiState.start();

                            } else {
                                // Stop the wifi state alert
                                mActivity.mAlertControlWifiState.stop(View.VISIBLE);
                            }

                        } else {
                            // Update wifi button icon
                            mActivity.mButtonWifi.setBackgroundResource(R.drawable.wifi_green);

                            // Check boat flask server not configured
                            if (!mFlaskServerConfigured) {

                                // Check boat flask server not configuring
                                if (mFlaskServerConfiguring) {
                                    // Start the wifi state alert
                                    mActivity.mAlertControlWifiState.start();

                                } else {
                                    // Stop the wifi state alert
                                    mActivity.mAlertControlWifiState.stop(View.VISIBLE);
                                }

                            } else {
                                // Stop the wifi state alert
                                mActivity.mAlertControlWifiState.stop(View.VISIBLE);
                            }
                        }
                    }
                }
            }
        });
    }
}
