package edu.vinc.bateau;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static java.time.LocalDateTime.now;

public class HttpRequester {

    private final Object mSyncObject;
    private final ConnectionManager mConnectionManager;

    private static final String LOG_TAG = HttpRequester.class.getName();

    public HttpRequester(ConnectionManager manager) {
        this.mConnectionManager = manager;
        this.mSyncObject = new Object();
    }

    public String sendRequest(String urlString, BoatManager.ServerType serverType) {
        // Set response to KO by default
        String response = "KO";

        // Create the request sending thread
        RequestSendingThread requestSendingThread = new RequestSendingThread(this.mConnectionManager, this.mSyncObject, urlString, serverType);

        // Start the request sending thread
        requestSendingThread.start();

        // Synchronize with the request sending thread
        synchronized (this.mSyncObject) {
            try {
                // Wait for request sending thread result
                this.mSyncObject.wait();

                // Get the response
                response = requestSendingThread.getResponse();

            } catch (InterruptedException ignored) {
            }
        }

        return response;
    }

    private static class RequestSendingThread extends Thread {

        private final Object mSyncObject;
        private final ConnectionManager mConnectionManager;
        private final String mUrlString;
        private final BoatManager.ServerType mBoatServer;
        private String response = "KO";

        public RequestSendingThread(ConnectionManager manager, Object syncObject, String urlString, BoatManager.ServerType serverType) {
            this.mConnectionManager = manager;
            this.mSyncObject = syncObject;
            this.mUrlString = urlString;
            this.mBoatServer = serverType;
        }

        public synchronized String getResponse() {
            return this.response;
        }

        @Override
        public void run(){
            synchronized(this.mSyncObject){
                try {
                    // Create URL from url string and open a connection
                    URL url = new URL("http://" + this.mConnectionManager.getBoatIp(this.mBoatServer) + this.mUrlString);
                    Log.e(LOG_TAG, now().toString() + " - sendRequestToBoatServer url: " + "http://" + this.mConnectionManager.getBoatIp(this.mBoatServer) + this.mUrlString);

                    // Open the network connection to url
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setReadTimeout(5000);
                    connection.setConnectTimeout(2000);
                    connection.setRequestMethod("GET");
                    connection.connect();

                    try {
                        // Check the response code
                        int responseCode = connection.getResponseCode();
                        if (responseCode != HttpsURLConnection.HTTP_OK) {
                            Log.e(LOG_TAG, now().toString() + " - RequestSendingThread HTTP error code: " + responseCode);

                            // Set server error
                            this.mConnectionManager.setFlaskServerError();
                            this.response = "KO - HTTP error code: " + responseCode;

                        } else {
                            // Retrieve the response body as an InputStream.
                            InputStream stream = connection.getInputStream();
                            StringBuilder buffer = new StringBuilder();
                            if (stream != null) {

                                // Converts Stream to String
                                String line;
                                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                                while ((line = reader.readLine()) != null) {
                                    buffer.append(line).append("\n");
                                }

                                // Check buffer not empty
                                if (buffer.length() == 0) {
                                    Log.e(LOG_TAG, now().toString() + " - RequestSendingThread null response !!!");

                                    // Set server error
                                    this.mConnectionManager.setFlaskServerError();
                                    this.response = "KO - null response";

                                } else {
                                    // Set result with buffer content
                                    this.response = buffer.toString();
                                }
                            }

                            // Close the stream
                            if (stream != null) {
                                stream.close();
                            }
                        }

                    } finally {
                        // Disconnect HTTP connection.
                        connection.disconnect();
                    }

                } catch (ProtocolException e) {
                    Log.e(LOG_TAG, now().toString() + " - sendRequestToBoatServer ProtocolException exception: " + e.getMessage());

                    // Set server error
                    this.mConnectionManager.setFlaskServerError();
                    this.response = "KO - ProtocolException exception: " + e.getMessage();

                } catch (IOException e) {
                    Log.e(LOG_TAG, now().toString() + " - sendRequestToBoatServer IOException exception: " + e.getMessage());

                    // Set server error
                    this.mConnectionManager.setFlaskServerError();
                    this.response = "KO - IOException exception: " + e.getMessage();
                }

                Log.e(LOG_TAG, now().toString() + " - sendRequestToBoatServer response: " + this.response);

                // Notify job finish
                this.mSyncObject.notify();
            }
        }
    }
}
