package edu.vinc.bateau;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

public class MainActivity extends AppCompatActivity implements Button.OnClickListener, SeekBar.OnSeekBarChangeListener, SharedPreferences.OnSharedPreferenceChangeListener {

    // Boat manager
    private BoatManager mBoatManager;

    // Shared preferences
    public SharedPreferences mSharedPrefs;
    private SharedPreferences.Editor mSharedPrefsEditor;

    // Main activity controls
    public SeekBar mSeekBarSpeed;
    public SeekBar mSeekBarDirection;
    public SeekBar mSeekBarTrimDirection;
    public TextView mTextViewBatteryVoltage;
    public ProgressBar mProgressBarBatteryVoltage;
    public TextView mTextViewMotorTemperature;
    public ProgressBar mProgressBarMotorTemperature;
    public TextView mTextViewCpuTemperature;
    public ProgressBar mProgressBarCpuTemperature;
    public TextView mTextViewWifiLevel;
    public ProgressBar mProgressBarWifiLevel;
    public Space mSpaceCamera;
    public Button mButtonCamera;
    public WebView mWebViewCamera;
    public Button mButtonWifi;

    // Alert managers
    public AlertControl mAlertControlWaterPresence;
    public AlertControl mAlertControlBatteryLowVoltage;
    public AlertControl mAlertControlWifiLowLevel;
    public AlertControl mAlertControlWifiState;
    public AlertControl mAlertControlCameraState;

    // Camera dialog controls
    private EditText mEditTextCameraWidth;
    private EditText mEditTextCameraHeight;
    private SeekBar mSeekBarCameraBrightness;
    private TextView mTextViewCameraBrightness;
    private SeekBar mSeekBarCameraContrast;
    private TextView mTextViewCameraContrast;
    private SeekBar mSeekBarCameraSaturation;
    private TextView mTextViewCameraSaturation;
    private SeekBar mSeekBarCameraHue;
    private TextView mTextViewCameraHue;
    private SeekBar mSeekBarCameraFrameRate;
    private TextView mTextViewCameraFrameRate;
    private SeekBar mSeekBarCameraPictureQuality;
    private TextView mTextViewCameraPictureQuality;
    private SeekBar mSeekBarCameraStreamMaxRate;
    private TextView mTextViewCameraStreamMaxRate;
    private SeekBar mSeekBarCameraStreamQuality;
    private TextView mTextViewCameraStreamQuality;

    private static boolean mApplicationRunning = false;
    private static boolean mSettingsOpen = false;
    private static boolean mPermissionsGranted = false;
    private static boolean mPermissionAlertDialogOpen = false;

    public static final int BOAT_PERMISSIONS_REQUEST_CODE = 1337;

    public static boolean getApplicationRunning() {
        return mApplicationRunning;
    }

    public static boolean getPermissionsGranted() {
        return mPermissionsGranted;
    }

    public static boolean getSettingsOpen() {
        return mSettingsOpen;
    }

    public static void setSettingsOpen(boolean settingsOpen) {
        mSettingsOpen = settingsOpen;
    }

    @SuppressLint({"CommitPrefEdits", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Make the application landscape fullscreen
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        this.setContentView(R.layout.activity_main);

        // Request permissions
        this.requestBoatPermissions();

        // Loads shared preferences
        this.mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.mSharedPrefsEditor = this.mSharedPrefs.edit();

        // Register shared preference change listener
        this.mSharedPrefs.registerOnSharedPreferenceChangeListener(this);

        // Settings button
        Button mButtonSettings = findViewById(R.id.buttonSettings);
        mButtonSettings.setOnClickListener(this);

        // Wifi button
        this.mButtonWifi = findViewById(R.id.buttonWifi);
        this.mButtonWifi.setOnClickListener(this);

        // Close button
        Button mButtonClose = findViewById(R.id.buttonClose);
        mButtonClose.setOnClickListener(this);

        // Battery voltage widget
        this.mTextViewBatteryVoltage = findViewById(R.id.textViewBatteryVoltage);
        this.mProgressBarBatteryVoltage = findViewById(R.id.progressBarBatteryVoltage);

        // Motor temperature widget
        this.mTextViewMotorTemperature = findViewById(R.id.textViewMotorTemperature);
        this.mProgressBarMotorTemperature = findViewById(R.id.progressBarMotorTemperature);

        // Cpu temperature widget
        this.mTextViewCpuTemperature = findViewById(R.id.textViewCpuTemperature);
        this.mProgressBarCpuTemperature = findViewById(R.id.progressBarCpuTemperature);

        // Wifi level widget
        this.mTextViewWifiLevel = findViewById(R.id.textViewWifiLevel);
        this.mProgressBarWifiLevel = findViewById(R.id.progressBarWifiLevel);

        // Water presence warning widget (hide by default)
        View mViewWaterPresence = findViewById(R.id.viewWaterPresence);
        mViewWaterPresence.setVisibility(View.INVISIBLE);

        // Battery alert widget (hide by default)
        View mViewBatteryLowVoltage = findViewById(R.id.viewBatteryLowVoltage);
        mViewBatteryLowVoltage.setVisibility(View.INVISIBLE);

        // Wifi level alert widget (hide by default)
        View mViewWifiLowLevel = findViewById(R.id.viewWifiLowLevel);
        mViewWifiLowLevel.setVisibility(View.INVISIBLE);

        // Speed seek bar
        this.mSeekBarSpeed = findViewById(R.id.seekBarSpeed);
        this.mSeekBarSpeed.setOnSeekBarChangeListener(this);

        // Direction seek bar
        this.mSeekBarDirection = findViewById(R.id.seekBarDirection);
        this.mSeekBarDirection.setOnSeekBarChangeListener(this);

        // Set direction limits
        this.mSeekBarDirection.setMin(Math.round(Float.parseFloat(this.mSharedPrefs.getString("direction-min-angle", "0.0"))));
        this.mSeekBarDirection.setMax(Math.round(Float.parseFloat(this.mSharedPrefs.getString("direction-max-angle", "180.0"))));
        this.mSeekBarDirection.setProgress(Math.round(Float.parseFloat(this.mSharedPrefs.getString("direction-initial-angle", "90.0"))));

        // Direction trim seek bar
        this.mSeekBarTrimDirection = findViewById(R.id.seekBarTrimDirection);
        this.mSeekBarTrimDirection.setOnSeekBarChangeListener(this);

        // Set trim seek bar progress with shared preference
        this.mSeekBarTrimDirection.setProgress(Integer.parseInt(mSharedPrefs.getString("client-default-trim", "0")));

        // Create the boat manager
        this.mBoatManager = new BoatManager(this);

        // Create the boat camera stream web view
        this.mWebViewCamera = findViewById(R.id.webViewCamera);
        this.mWebViewCamera.setWebViewClient(this.mBoatManager.getCameraWebViewClient());

        // Camera button and space
        this.mSpaceCamera = findViewById(R.id.spaceCamera);
        this.mButtonCamera = findViewById(R.id.buttonCamera);
        this.mButtonCamera.setOnClickListener(this);

        // Alert controls
        this.mAlertControlWaterPresence = new AlertControl(this, mViewWaterPresence);
        this.mAlertControlBatteryLowVoltage = new AlertControl(this, mViewBatteryLowVoltage);
        this.mAlertControlWifiLowLevel = new AlertControl(this, mViewWifiLowLevel);
        this.mAlertControlWifiState = new AlertControl(this, this.mButtonWifi);
        this.mAlertControlCameraState = new AlertControl(this, this.mButtonCamera);

        // Update the camera button
        this.updateCameraButton();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Set application running
        mApplicationRunning = true;

        // Start the boat manager
        this.mBoatManager.start();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        // Unregister shared preference change listener
        this.mSharedPrefs.unregisterOnSharedPreferenceChangeListener(this);

        // Reset application running
        mApplicationRunning = false;

        // Stop the boat manager (close the boat connection)
        this.mBoatManager.stop(true);
    }

    @Override
    protected void onPause(){
        super.onPause();

        // Stop the boat manager (don't close the boat connection)
        this.mBoatManager.stop(false);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        // Get value as string of changed preference
        String value = "";
        if (mSharedPrefs.getAll().get(key) instanceof Boolean) {
            value = String.valueOf(mSharedPrefs.getBoolean(key,false));
        } else if (mSharedPrefs.getAll().get(key) instanceof Integer) {
            value = String.valueOf(mSharedPrefs.getInt(key, 0));
        } else if (mSharedPrefs.getAll().get(key) instanceof String) {
            value = mSharedPrefs.getString(key, "");
        }

        // Check if a client preference
        if (key.startsWith("client")) {
            //Check if trim direction seek bar preference
            if (key.endsWith("default-trim")) {
                // Set trim seek bar progress with shared preference
                mSeekBarTrimDirection.setProgress(Integer.parseInt(mSharedPrefs.getString("client-default-trim", "0")));
            }
        }

        // Boat preferences
        else {
            // Check if a camera preference
            if (key.startsWith("camera")) {
                // Check if camera activated preference
                if (key.endsWith("activated")) {
                    // Update the camera button
                    updateCameraButton();

                    // Restart the camera manager
                    this.mBoatManager.restartCameraManager(0);
                }

                // Other camera preference
                else {
                    // Send camera preference to the boat motion server
                    mBoatManager.sendSettingToBoatServer(key, value);
                }
            }

            // Other boat preference
            else {
                // Send other settings to the boat flask server
                mBoatManager.sendSettingToBoatServer(key, value);
            }
        }
    }

    public void requestBoatPermissions() {
        //Check permissions granted
        if (!mPermissionsGranted
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED)
        {
            //Request permissions
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.CHANGE_WIFI_STATE,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.CHANGE_NETWORK_STATE},
                    MainActivity.BOAT_PERMISSIONS_REQUEST_CODE);

        } else {
            // Set permissions granted flag
            mPermissionsGranted = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        // Check all permissions granted
        if (requestCode == BOAT_PERMISSIONS_REQUEST_CODE) {

            // Check location service disabled
            boolean permissionDenied = false;
            for (int result : grantResults) {

                // Check if a permission is denied
                if (result == PackageManager.PERMISSION_DENIED) {
                    permissionDenied = true;

                    break;
                }
            }

            // Check permission denied
            if (permissionDenied) {

                // Check permission alert dialog not already open
                if (!mPermissionAlertDialogOpen) {

                    // Set permission alert dialog open
                    mPermissionAlertDialogOpen = true;

                    // Open a dialog to ask to allow location service for the app
                    new AlertDialog.Builder(this)
                            .setTitle("Warning")
                            .setMessage("Please enable location service")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Open the location settings
                                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    mBoatManager.mActivity.startActivity(myIntent);

                                    // Reset permission alert dialog open
                                    mPermissionAlertDialogOpen = false;
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Reset permission alert dialog open
                                    mPermissionAlertDialogOpen = false;
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }

            } else {
                // Check all other permissions
                boolean permissionsGranted = true;
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        permissionsGranted = false;
                        break;
                    }
                }

                // Check all permissions granted
                if (permissionsGranted) {
                    // Set permissions granted flag
                    mPermissionsGranted = true;

                } else {
                    // Reset permissions granted flag
                    mPermissionsGranted = false;

                    // Permissions not granted
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Some permissions are not granted...", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        // Switch calling button
        switch (view.getId()) {
            // Setting button
            case R.id.buttonSettings: {
                // Open the setting window
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);

                break;
            }

            // Close button
            case R.id.buttonClose: {
                // Close the application
                finishAndRemoveTask();

                break;
            }

            // Wifi button
            case R.id.buttonWifi: {
                // Open wifi dialog to ask if user want restart wifi
                new AlertDialog.Builder(this)
                        .setTitle("Boat Wifi")
                        .setMessage("Restart the wifi connection ?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Stop the boat manager
                                mBoatManager.stop(true);

                                // Start the boat manager
                                mBoatManager.start();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                break;
            }

            // Camera button
            case R.id.buttonCamera: {
                // Create the camera dialog
                Dialog mDialogCamera = new Dialog(this);
                LayoutInflater inflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);
                if (inflater != null) {

                    // Set camera dialog content
                    View layout = inflater.inflate(R.layout.camera_dialog, (ViewGroup) findViewById(R.id.cameraDialogLayout));
                    mDialogCamera.setContentView(layout);

                    // Camera dialog controls
                    Button mButtonCameraApply = mDialogCamera.findViewById(R.id.buttonCameraApply);
                    mButtonCameraApply.setOnClickListener(this);
                    this.mEditTextCameraWidth = mDialogCamera.findViewById(R.id.editTextCameraWidth);
                    this.mEditTextCameraWidth.setText(mSharedPrefs.getString("camera-width", "728")); // Multiple of 8
                    this.mEditTextCameraHeight = mDialogCamera.findViewById(R.id.editTextCameraHeight);
                    this.mEditTextCameraHeight.setText(mSharedPrefs.getString("camera-height", "304")); // Multiple of 8
                    this.mSeekBarCameraBrightness = mDialogCamera.findViewById(R.id.seekBarCameraBrightness);
                    this.mSeekBarCameraBrightness.setProgress(Integer.parseInt(mSharedPrefs.getString("camera-brightness", "0"))); // 0 to 255 - 0 = Default
                    this.mSeekBarCameraBrightness.setOnSeekBarChangeListener(this);
                    this.mTextViewCameraBrightness = mDialogCamera.findViewById(R.id.textViewCameraBrightness);
                    this.mTextViewCameraBrightness.setText(mSharedPrefs.getString("camera-brightness", "0"));
                    this.mSeekBarCameraContrast = mDialogCamera.findViewById(R.id.seekBarCameraContrast);
                    this.mSeekBarCameraContrast.setProgress(Integer.parseInt(mSharedPrefs.getString("camera-contrast", "0"))); // 0 to 255 - 0 = Default
                    this.mSeekBarCameraContrast.setOnSeekBarChangeListener(this);
                    this.mTextViewCameraContrast = mDialogCamera.findViewById(R.id.textViewCameraContrast);
                    this.mTextViewCameraContrast.setText(mSharedPrefs.getString("camera-contrast", "0"));
                    this.mSeekBarCameraSaturation = mDialogCamera.findViewById(R.id.seekBarCameraSaturation);
                    this.mSeekBarCameraSaturation.setProgress(Integer.parseInt(mSharedPrefs.getString("camera-saturation", "0"))); // 0 to 255 - 0 = Default
                    this.mSeekBarCameraSaturation.setOnSeekBarChangeListener(this);
                    this.mTextViewCameraSaturation = mDialogCamera.findViewById(R.id.textViewCameraSaturation);
                    this.mTextViewCameraSaturation.setText(mSharedPrefs.getString("camera-saturation", "0"));
                    this.mSeekBarCameraHue = mDialogCamera.findViewById(R.id.seekBarCameraHue);
                    this.mSeekBarCameraHue.setProgress(Integer.parseInt(mSharedPrefs.getString("camera-hue", "0"))); // 0 to 255 - 0 = Default
                    this.mSeekBarCameraHue.setOnSeekBarChangeListener(this);
                    this.mTextViewCameraHue = mDialogCamera.findViewById(R.id.textViewCameraHue);
                    this.mTextViewCameraHue.setText(mSharedPrefs.getString("camera-hue", "0"));
                    this.mSeekBarCameraFrameRate = mDialogCamera.findViewById(R.id.seekBarCameraFrameRate);
                    this.mSeekBarCameraFrameRate.setProgress(Integer.parseInt(mSharedPrefs.getString("camera-framerate", "100"))); // 2 to 100
                    this.mSeekBarCameraFrameRate.setOnSeekBarChangeListener(this);
                    this.mTextViewCameraFrameRate = mDialogCamera.findViewById(R.id.textViewCameraFrameRate);
                    this.mTextViewCameraFrameRate.setText(mSharedPrefs.getString("camera-framerate", "25"));
                    this.mSeekBarCameraPictureQuality = mDialogCamera.findViewById(R.id.seekBarCameraPictureQuality);
                    this.mSeekBarCameraPictureQuality.setProgress(Integer.parseInt(mSharedPrefs.getString("camera-quality", "100"))); // 1 to 100
                    this.mSeekBarCameraPictureQuality.setOnSeekBarChangeListener(this);
                    this.mTextViewCameraPictureQuality = mDialogCamera.findViewById(R.id.textViewCameraPictureQuality);
                    this.mTextViewCameraPictureQuality.setText(mSharedPrefs.getString("camera-quality", "50"));
                    this.mSeekBarCameraStreamMaxRate = mDialogCamera.findViewById(R.id.seekBarCameraStreamMaxRate);
                    this.mSeekBarCameraStreamMaxRate.setProgress(Integer.parseInt(mSharedPrefs.getString("camera-stream-maxrate", "100"))); // 1 to 100
                    this.mSeekBarCameraStreamMaxRate.setOnSeekBarChangeListener(this);
                    this.mTextViewCameraStreamMaxRate = mDialogCamera.findViewById(R.id.textViewCameraStreamMaxRate);
                    this.mTextViewCameraStreamMaxRate.setText(mSharedPrefs.getString("camera-stream-maxrate", "25"));
                    this.mSeekBarCameraStreamQuality = mDialogCamera.findViewById(R.id.seekBarCameraStreamQuality);
                    this.mSeekBarCameraStreamQuality.setProgress(Integer.parseInt(mSharedPrefs.getString("camera-stream-quality", "100"))); // 1 to 100
                    this.mSeekBarCameraStreamQuality.setOnSeekBarChangeListener(this);
                    this.mTextViewCameraStreamQuality = mDialogCamera.findViewById(R.id.textViewCameraStreamQuality);
                    this.mTextViewCameraStreamQuality.setText(mSharedPrefs.getString("camera-stream-quality", "50"));
                }

                // Show the camera dialog
                mDialogCamera.show();

                break;
            }

            // Camera apply button
            case R.id.buttonCameraApply: {
                // Save camera setting if changed
                if (!String.valueOf(this.mEditTextCameraWidth.getText()).equals(this.mSharedPrefs.getString("camera-width", "728"))) {
                    // Save the new value
                    this.mSharedPrefsEditor.putString("camera-width", String.valueOf(this.mEditTextCameraWidth.getText()));
                    this.mSharedPrefsEditor.apply();
                }
                if (!String.valueOf(this.mEditTextCameraHeight.getText()).equals(this.mSharedPrefs.getString("camera-height", "304"))) {
                    // Save the new value
                    this.mSharedPrefsEditor.putString("camera-height", String.valueOf(this.mEditTextCameraHeight.getText()));
                    this.mSharedPrefsEditor.apply();
                }
                if (this.mSeekBarCameraBrightness.getProgress() != Integer.parseInt(mSharedPrefs.getString("camera-brightness", "0"))) {
                    // Save the new value
                    this.mSharedPrefsEditor.putString("camera-brightness", String.valueOf(this.mSeekBarCameraBrightness.getProgress()));
                    this.mSharedPrefsEditor.apply();
                }
                if (this.mSeekBarCameraContrast.getProgress() != Integer.parseInt(mSharedPrefs.getString("camera-contrast", "0"))) {
                    // Save the new value
                    this.mSharedPrefsEditor.putString("camera-contrast", String.valueOf(this.mSeekBarCameraContrast.getProgress()));
                    this.mSharedPrefsEditor.apply();
                }
                if (this.mSeekBarCameraSaturation.getProgress() != Integer.parseInt(mSharedPrefs.getString("camera-saturation", "0"))) {
                    // Save the new value
                    this.mSharedPrefsEditor.putString("camera-saturation", String.valueOf(this.mSeekBarCameraSaturation.getProgress()));
                    this.mSharedPrefsEditor.apply();
                }
                if (this.mSeekBarCameraHue.getProgress() != Integer.parseInt(mSharedPrefs.getString("camera-hue", "0"))) {
                    // Save the new value
                    this.mSharedPrefsEditor.putString("camera-hue", String.valueOf(this.mSeekBarCameraHue.getProgress()));
                    this.mSharedPrefsEditor.apply();
                }
                if (this.mSeekBarCameraFrameRate.getProgress() != Integer.parseInt(mSharedPrefs.getString("camera-framerate", "25"))) {
                    // Save the new value
                    this.mSharedPrefsEditor.putString("camera-framerate", String.valueOf(this.mSeekBarCameraFrameRate.getProgress()));
                    this.mSharedPrefsEditor.apply();
                }
                if (this.mSeekBarCameraPictureQuality.getProgress() != Integer.parseInt(mSharedPrefs.getString("camera-quality", "50"))) {
                    // Save the new value
                    this.mSharedPrefsEditor.putString("camera-quality", String.valueOf(this.mSeekBarCameraPictureQuality.getProgress()));
                    this.mSharedPrefsEditor.apply();
                }
                if (this.mSeekBarCameraStreamMaxRate.getProgress() != Integer.parseInt(mSharedPrefs.getString("camera-stream-maxrate", "25"))) {
                    // Save the new value
                    this.mSharedPrefsEditor.putString("camera-stream-maxrate", String.valueOf(this.mSeekBarCameraStreamMaxRate.getProgress()));
                    this.mSharedPrefsEditor.apply();
                }
                if (this.mSeekBarCameraStreamQuality.getProgress() != Integer.parseInt(mSharedPrefs.getString("camera-stream-quality", "50"))) {
                    // Save the new value
                    this.mSharedPrefsEditor.putString("camera-stream-quality", String.valueOf(this.mSeekBarCameraStreamQuality.getProgress()));
                    this.mSharedPrefsEditor.apply();
                }

                break;
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
        // Switch calling seek bar
        switch (seekBar.getId()) {
            // Direction trim seek bar
            case R.id.seekBarTrimDirection: {
                // Set shared preference with trim seek bar progress
                this.mSharedPrefsEditor.putString("client-default-trim", String.valueOf(progressValue));
                this.mSharedPrefsEditor.apply();
                break;
            }

            // Camera brightness seek bar
            case R.id.seekBarCameraBrightness: {
                // Update camera brightness text view
                this.mTextViewCameraBrightness.setText(String.valueOf(progressValue));
                break;
            }

            // Camera contrast seek bar
            case R.id.seekBarCameraContrast: {
                // Update camera contrast text view
                this.mTextViewCameraContrast.setText(String.valueOf(progressValue));
                break;
            }

            // Camera saturation seek bar
            case R.id.seekBarCameraSaturation: {
                // Update camera saturation text view
                this.mTextViewCameraSaturation.setText(String.valueOf(progressValue));
                break;
            }

            // Camera hue seek bar
            case R.id.seekBarCameraHue: {
                // Update camera hue text view
                this.mTextViewCameraHue.setText(String.valueOf(progressValue));
                break;
            }

            // Camera frame rate seek bar
            case R.id.seekBarCameraFrameRate: {
                // Update camera frame rate text view
                this.mTextViewCameraFrameRate.setText(String.valueOf(progressValue));
                break;
            }

            // Camera picture quality seek bar
            case R.id.seekBarCameraPictureQuality: {
                // Update camera picture quality text view
                this.mTextViewCameraPictureQuality.setText(String.valueOf(progressValue));
                break;
            }

            // Camera stream max rate seek bar
            case R.id.seekBarCameraStreamMaxRate: {
                // Update camera stream max rate text view
                this.mTextViewCameraStreamMaxRate.setText(String.valueOf(progressValue));
                break;
            }

            // Camera stream quality seek bar
            case R.id.seekBarCameraStreamQuality: {
                // Update camera stream quality text view
                this.mTextViewCameraStreamQuality.setText(String.valueOf(progressValue));
                break;
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // Switch calling seek bar
        switch (seekBar.getId()) {
            // Seep seek bar
            case R.id.seekBarSpeed:
            {
                // Replace the speed seek bar at median position
                this.mSeekBarSpeed.setProgress(0);
                break;
            }
            // Direction seek bar
            case R.id.seekBarDirection:
            {
                // Replace the direction seek bar at median position
                this.mSeekBarDirection.setProgress(Math.round(Float.parseFloat(this.mSharedPrefs.getString("direction-initial-angle", "90.0"))));
                break;
            }
        }
    }

    private void updateCameraButton() {
        // Check if the boat camera is activated
        if (mSharedPrefs.getBoolean("camera-activated", false)) {
            // Show the camera button
            mSpaceCamera.setVisibility(Space.VISIBLE);
            mButtonCamera.setVisibility(Button.VISIBLE);

            // Update the camera state
            this.mBoatManager.updateCameraState();

        } else {
            // Hide the camera button
            mSpaceCamera.setVisibility(Space.GONE);
            mButtonCamera.setVisibility(Button.GONE);
        }
    }
}