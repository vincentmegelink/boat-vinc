package edu.vinc.bateau;

import android.annotation.SuppressLint;
import android.view.View;

public class StatusManager extends BaseManager {

    public StatusManager(BoatManager manager) {
        super(manager);
    }

    @Override
    protected void execute() {
        // Check boat ready for status request
        if (this.mBoatManager.isReadyForStatus()) {

            // Send status request to the boat server
            final String response = this.mBoatManager.sendRequestToBoatServer("/boat/status", BoatManager.ServerType.FLASK);

            // Check response
            if (!response.startsWith("OK")) {
                // Set boat flask server error
                this.mBoatManager.setFlaskServerError();

                // Reset boat flask server status Ok
                this.mBoatManager.setFlaskServerStatusOk(false);

            } else {
                // Check first status ok after connection
                if (!this.mBoatManager.isReadyForCommand()) {
                    // Restart all managers to inform of boat flask server status ok
                    this.mBoatManager.restartAllManagers(0);
                }

                // Set boat flask server status Ok
                this.mBoatManager.setFlaskServerStatusOk(true);

                // Update UI
                this.mActivity.runOnUiThread(new Runnable() {
                    @SuppressLint("DefaultLocale")
                    public void run() {
                        // Extract info from response - Ex: OK - status : batteryVoltage = 0.000976681723843 motorTemperature = 25.7319678311
                        String[] split = response.split(":");
                        String status = split[1].trim();
                        split = status.split(";");

                        // Update the battery voltage
                        String batteryVoltage = split[0].replace("batteryVoltage =", "").trim();
                        mActivity.mProgressBarBatteryVoltage.setProgress(Math.round(Float.parseFloat(batteryVoltage) * 10));
                        mActivity.mTextViewBatteryVoltage.setText(String.format("Battery: %.02f v", Float.parseFloat(batteryVoltage)));

                        // Check low voltage alert
                        if (Float.parseFloat(batteryVoltage) < Float.parseFloat(mActivity.mSharedPrefs.getString("client-battery-low-voltage", "7.0"))) {
                            // Start the low battery voltage alert
                            mActivity.mAlertControlBatteryLowVoltage.start();

                        } else {
                            // Stop the low battery voltage alert
                            mActivity.mAlertControlBatteryLowVoltage.stop(View.INVISIBLE);
                        }

                        // Update the motor temperature
                        String motorTemperature = split[1].replace("motorTemperature =", "").trim();
                        mActivity.mProgressBarMotorTemperature.setProgress(Math.round(Float.parseFloat(motorTemperature)));
                        mActivity.mTextViewMotorTemperature.setText(String.format("Motor: %.02f °", Float.parseFloat(motorTemperature)));

                        // Update the cpu temperature
                        String cpuTemperature = split[2].replace("cpuTemperature =", "").trim();
                        mActivity.mProgressBarCpuTemperature.setProgress(Math.round(Float.parseFloat(cpuTemperature)));
                        mActivity.mTextViewCpuTemperature.setText(String.format("Cpu: %.02f °", Float.parseFloat(cpuTemperature)));

                        // Update the wifi level
                        int wifiLevel = mBoatManager.getBoatWifiLevel();
                        mActivity.mProgressBarWifiLevel.setProgress(wifiLevel);
                        mActivity.mTextViewWifiLevel.setText(String.format("Wifi: %d %%", wifiLevel));

                        // Check wifi low level
                        if (wifiLevel < Integer.parseInt(mActivity.mSharedPrefs.getString("client-wifi-low-level", "10"))) {
                            // Start the low wifi level alert
                            mActivity.mAlertControlWifiLowLevel.start();

                        } else {
                            // Stop the low wifi level alert
                            mActivity.mAlertControlWifiLowLevel.stop(View.INVISIBLE);
                        }

                        // Check water presence
                        String waterPresence = split[3].replace("waterPresence =", "").trim();
                        if (waterPresence.equals("1")) {
                            // Start the water presence alert
                            mActivity.mAlertControlWaterPresence.start();

                        } else {
                            // Stop the water presence alert
                            mActivity.mAlertControlWaterPresence.stop(View.INVISIBLE);
                        }
                    }
                });
            }
        }
    }

    @Override
    public void stop(final boolean forceStop) {
        super.stop(forceStop);

        // Check force stop
        if (forceStop) {
            // Stop the low battery voltage alert
            this.mActivity.mAlertControlBatteryLowVoltage.stop(View.INVISIBLE);

            // Stop the water presence alert
            this.mActivity.mAlertControlWaterPresence.stop(View.INVISIBLE);

            // Stop the wifi low level alert
            this.mActivity.mAlertControlWifiLowLevel.stop(View.INVISIBLE);
        }
    }

    @Override
    protected long getDelay() {
        return Integer.parseInt(this.mActivity.mSharedPrefs.getString("client-status-refresh-rate", "1000"));
    }

    @Override
    public void resetConnection(final boolean updateState) { }
}
