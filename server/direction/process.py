#!/usr/bin/env python
#-- coding: utf-8 --

import os
import sys
import time
import datetime
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from processmanager import ProcessManager
from gpiozero import AngularServo

# Create a angular servo instance
servoPIN = 14
servoInitialAngle = 90.0
servoMinAngle = 0.0
servoMaxAngle = 180.0
servoMinPulse = 0.0008 # 0.8ms
servoMaxPulse = 0.0020 # 2.0ms
servoFrameWidth = 0.02 # 20.0ms
servo = AngularServo(servoPIN, \
                     initial_angle=servoInitialAngle, \
                     min_angle=servoMinAngle, \
                     max_angle=servoMaxAngle, \
                     min_pulse_width=servoMinPulse, \
                     max_pulse_width=servoMaxPulse, \
                     frame_width=servoFrameWidth, \
                     pin_factory=None)

# Initialize variables
current = 0
lastMoveTime = datetime.datetime.now()

# Create the process manager in client mode to communicate with the boat server
processManager = ProcessManager("direction", serverMode=False, clientPort=6003, serverPort=6002)

# Start the ipc communication with the boat server
processManager.start()

while True:
    try:
        # Check if a message from the boat server is available for 2 sec
        if processManager.poll(2):
            # Read the message from the boat server
            msg = processManager.receive()
            #print ("Direction: Received message: %s" % msg)

            # Check message
            if msg[0] == "move":
                # Read target angle 0° - 180°
                target = int(msg[1])
                #print ("Direction: Received target = %d" % target)

                # Check target angle not equal to current angle
                if (current <> target):
                    # Update the servo angle
                    servo.angle = target

                    # Save last move time
                    lastMoveTime = datetime.datetime.now()

                # Target angle equal current angle
                else:
                    # Check time elapsed since last move
                    diffTime = datetime.datetime.now() - lastMoveTime
                    # Let at least 200 ms to the servo to finish the move before stopping it
                    if diffTime.microseconds > 200 * 1000:
                        # Stop the servo
                        servo.angle = None

                # Save target angle in current angle
                current = target

            # Check if a set message
            elif msg[0] == "set":
                needRestart = False;
                print ("Direction: Received setting [key=%s, value=%s]" % (msg[1], msg[2]))
                # Check if the message is pin setting
                if msg[1].endswith("pin"):
                    # Check new setting not equal to current setting
                    if servoPIN <> int(msg[2]):
                        print ("Direction: Setting [key=%s, current_value=%d, new_value=%s]" % (msg[1], servoPIN, msg[2]))
                        # Update the servo pin
                        servoPIN = int(msg[2])
                        # Set need to restart device
                        needRestart = True;

                # Check if the message is initial angle setting
                elif msg[1].endswith("initial-angle"):
                    # Check new setting not equal to current setting
                    if servoInitialAngle <> float(msg[2]):
                        print ("Direction: Setting [key=%s, current_value=%.02f, new_value=%s]" % (msg[1], servoInitialAngle, msg[2]))
                        # Update the servo initial angle
                        servoInitialAngle = float(msg[2])
                        # Set need to restart device
                        needRestart = True;

                # Check if the message is min angle setting
                elif msg[1].endswith("min-angle"):
                    # Check new setting not equal to current setting
                    if servoMinAngle <> float(msg[2]):
                        print ("Direction: Setting [key=%s, current_value=%.02f, new_value=%s]" % (msg[1], servoMinAngle, msg[2]))
                        # Update the servo min angle
                        servoMinAngle = float(msg[2])
                        # Set need to restart device
                        needRestart = True;

                # Check if the message is max angle setting
                elif msg[1].endswith("max-angle"):
                    # Check new setting not equal to current setting
                    if servoMaxAngle <> float(msg[2]):
                        print ("Direction: Setting [key=%s, current_value=%.02f, new_value=%s]" % (msg[1], servoMaxAngle, msg[2]))
                        # Update the servo max angle
                        servoMaxAngle = float(msg[2])
                        # Set need to restart device
                        needRestart = True;

                # Check if the message is min pulse width setting
                elif msg[1].endswith("min-pulse-width"):
                    # Check new setting not equal to current setting
                    if servoMinPulse <> float(msg[2]) / 1000.0:
                        print ("Direction: Setting [key=%s, current_value=%.02f, new_value=%s]" % (msg[1], servoMinPulse, msg[2]))
                        # Update the servo min pulse width
                        servoMinPulse = float(msg[2]) / 1000.0
                        # Set need to restart device
                        needRestart = True;

                # Check if the message is max pulse width setting
                elif msg[1].endswith("max-pulse-width"):
                    # Check new setting not equal to current setting
                    if servoMaxPulse <> float(msg[2]) / 1000.0:
                        print ("Direction: Setting [key=%s, current_value=%.02f, new_value=%s]" % (msg[1], servoMaxPulse, msg[2]))
                        # Update the servo max pulse width
                        servoMaxPulse = float(msg[2]) / 1000.0
                        # Set need to restart device
                        needRestart = True;

                # Check if the message is frame width setting
                elif msg[1].endswith("frame-width"):
                    # Check new setting not equal to current setting
                    if servoFrameWidth <> float(msg[2]) / 1000.0:
                        print ("Direction: Setting [key=%s, current_value=%.02f, new_value=%s]" % (msg[1], servoFrameWidth, msg[2]))
                        # Update the servo frame width
                        servoFrameWidth = float(msg[2]) / 1000.0
                        # Set need to restart device
                        needRestart = True;

                # Check if need to restart the device to apply the settings
                if needRestart:
                    print ("Direction: Restarting the AngularServo")
                    # Close the servo
                    servo.close()

                    # Create a new angular servo instance
                    servo = AngularServo(servoPIN, \
                                         initial_angle=servoInitialAngle, \
                                         min_angle=servoMinAngle, \
                                         max_angle=servoMaxAngle, \
                                         min_pulse_width=servoMinPulse, \
                                         max_pulse_width=servoMaxPulse, \
                                         frame_width=servoFrameWidth, \
                                         pin_factory=None)

            # Check if a stop message
            elif msg[0] == "stop":
                print ("Direction: Received stop")
                # Close the servo
                servo.close()

                # Send OK message to the boat server
                processManager.send(["OK"])

                # Close all ipc connection
                processManager.close()

                # Break the main loop
                break

        # Not move message for more then 2 seconds => place the servo in mid position
        else:
            # Check if the servo is already at mid position
            if current <> servoInitialAngle:
                # Place the servo in mid position
                servo.angle = servoInitialAngle

                # Save current angle
                current = servoInitialAngle

                # Wait 1 second before stopping the servo
                time.sleep(1)

            # Stop the servo
            servo.angle = None

    except ValueError, e:
        print ("Direction: Exception !!! message:" + str(e))

    except KeyboardInterrupt:
        print ("Direction: KeyboardInterrupt !!!")
        # Close the servo
        servo.close()
        break
