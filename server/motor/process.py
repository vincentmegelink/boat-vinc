#!/usr/bin/env python
#-- coding: utf-8 --

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from processmanager import ProcessManager
from gpiozero import PWMOutputDevice, DigitalOutputDevice

# Create a pwm ouput device instance for the motor speed
motorPIN = 18
motorFrequency = 600
motor = PWMOutputDevice(motorPIN, \
                        active_high=True, \
                        initial_value=0, \
                        frequency=motorFrequency, \
                        pin_factory=None)

# Create a digital ouput device instance for the motor reverse
reversePIN = 23
reverseActiveHigh = True
reverse = DigitalOutputDevice(reversePIN, \
                              active_high=reverseActiveHigh,
                              initial_value=False,
                              pin_factory=None)

# Create the process manager in client mode to communicate with the boat server
processManager = ProcessManager("motor", serverMode=False, clientPort=6001, serverPort=6000)

# Start the ipc communication with the boat server
processManager.start()

while True:
    try:
        # Check if a message from the boat server is available for 2 sec
        if processManager.poll(2):
            # Read the message from the boat server
            msg = processManager.receive()
            #print ("Motor: Received message: %s" % msg)

            # Check message
            if msg[0] == "move":
                # Read target speed -100% - 100%
                target = int(msg[1])
                #print ("Motor: Received target = %d" % target)

                # Check forward/backward mode
                if target > 0:
                    reverse.off()
                elif target < 0:
                    reverse.on()

                # Update the motor pwm dutycycle (0.0 - 1.0)
                dutycycle = abs(target) / 100.0
                motor.value = dutycycle
                #print ("Motor: Speed (target = %d, dutycycle = %.02f" % (target, dutycycle))

            # Check if a settings message
            elif msg[0] == "set":
                print ("Motor: Received setting [key=%s, value=%s]" % (msg[1], msg[2]))
                motorSetting = False
                reverseSetting = False
                # Check if the message is pin setting
                if msg[1].endswith("motor-pin"):
                    # Check new setting not equal to current setting
                    if motorPIN <> int(msg[2]):
                        print ("Motor: Setting [key=%s, current_value=%d, new_value=%s]" % (msg[1], motorPIN, msg[2]))
                        # Update the motor pin
                        motorPIN = int(msg[2])
                        motorSetting = True

                # Check if the message is frequency setting
                elif msg[1].endswith("frequency"):
                    # Check new setting not equal to current setting
                    if motorFrequency <> int(msg[2]):
                        print ("Motor: Setting [key=%s, current_value=%d, new_value=%s]" % (msg[1], motorFrequency, msg[2]))
                        # Update the motor frequency
                        motorFrequency = int(msg[2])
                        motorSetting = True

                # Check if the message is reverse pin setting
                elif msg[1].endswith("reverse-pin"):
                    # Check new setting not equal to current setting
                    if reversePIN <> int(msg[2]):
                        print ("Motor: Setting [key=%s, current_value=%d, new_value=%s]" % (msg[1], reversePIN, msg[2]))
                        # Update the motor reverse pin
                        reversePIN = int(msg[2])
                        reverseSetting = True

                # Check if the message is reverse active high setting
                elif msg[1].endswith("reverse-active-high"):
                    # Check new setting not equal to current setting
                    if reverseActiveHigh <> ((msg[2] == "true") or (msg[2] == "True")):
                        print ("Motor: Setting [key=%s, current_value=%d, new_value=%s]" % (msg[1], reverseActiveHigh, msg[2]))
                        # Update the motor reverse active high
                        reverseActiveHigh = (msg[2] == "true") or (msg[2] == "True")
                        reverseSetting = True

                # Check if a motor setting => Need to restart the device to apply the settings
                if motorSetting:
                    print ("Motor: Restarting the PWMOutputDevice")
                    # Close the motor
                    motor.close()

                    # Create a new pwm ouput device instance for the motor speed
                    motor = PWMOutputDevice(motorPIN, \
                                            active_high=True, \
                                            initial_value=0, \
                                            frequency=motorFrequency, \
                                            pin_factory=None)

                # Check if a reverse setting => Need to restart the device to apply the settings
                if reverseSetting:
                    print ("Motor: Restarting the DigitalOutputDevice")
                    # Close the motor inverter
                    reverse.close()

                    # Create a new digital ouput device instance for the motor reverse
                    reverse = DigitalOutputDevice(reversePIN, \
                                                  active_high=reverseActiveHigh,
                                                  initial_value=False,
                                                  pin_factory=None)

            # Check if a stop message
            elif msg[0] == "stop":
                print ("Motor: Received stop")
                # Close the motor and the inverter
                motor.close()
                reverse.close()

                # Send OK message to the server
                processManager.send(["OK"])

                # Close all ipc connection
                processManager.close()

                # Break the main loop
                break

        else:
            # Stop the motor if no message received from the boat server after 2 seconds
            motor.off()
            reverse.off()

    except ValueError, e:
        print ("Motor: Exception !!! message:" + str(e))

    except KeyboardInterrupt:
        print ("Motor: KeyboardInterrupt !!!")
        # Stop the motor
        motor.close()
        reverse.close()
        break
