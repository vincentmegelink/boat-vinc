#!/usr/bin/env python
#-- coding: utf-8 --

import subprocess
from multiprocessing.connection import Client, Listener

class ProcessManager:
    def __init__(self, name, serverMode=True, clientPort=6000, serverPort=6001):
#        print("ProcessManager: __init__ [" + \
#              "name=" + name + \
#              ", serverMode=" + str(serverMode) + \
#              ", clientPort=" + str(clientPort) + \
#              ", serverPort=" + str(serverPort) + \
#              "]")
        self.name = name
        self.serverMode = serverMode
        self.authkey = 'boat-server-1337'
        self.clientAddress = ('localhost', clientPort)
        self.serverAddress = ('localhost', serverPort)
        self.process = None
        self.listener = None
        self.serverConnection = None
        self.clientConnection = None

    def isActive(self):
        return not (self.process is None or \
                    self.listener is None or \
                    self.serverConnection is None or \
                    self.clientConnection is None)

    def setPort(self, clientPort, serverPort):
        self.clientAddress = ('localhost', clientPort)
        self.serverAddress = ('localhost', serverPort)

    def start(self):
        # Check if start in server mode
        if self.serverMode:
            print("ProcessManager: Boat server start [" + \
                  "name=" + self.name + \
                  "]")
            # Start the process in a new subprocess
            if self.process is None:
                self.process = subprocess.Popen(["sudo", "python", self.name + "/process.py"])

            # Start the server ipc with the process
            if self.serverConnection is None:
                self.listener = Listener(self.serverAddress, authkey=self.authkey)
                self.serverConnection = self.listener.accept()
#                print ("ProcessManager: Boat Server accept connection from process " + self.name)

            # Start the client ipc with the process
            if self.clientConnection is None:
                self.clientConnection = Client(self.clientAddress, authkey=self.authkey)
#                print ("ProcessManager: Boat Server connecting process " + self.name)

        # Start in client mode
        else:
            print("ProcessManager: Process start [" + \
                  "name=" + self.name + \
                  "]")
            # Start the client ipc with the process
            if self.clientConnection is None:
                self.clientConnection = Client(self.clientAddress, authkey=self.authkey)
#                print ("ProcessManager: Process " + self.name + " connecting boat server...")

            # Start the server ipc with the process
            if self.serverConnection is None:
                self.listener = Listener(self.serverAddress, authkey=self.authkey)
                self.serverConnection = self.listener.accept()
#                print ("ProcessManager: Process " + self.name + " accept connection from boat server")

    def stop(self):
        if self.serverMode:
            print("ProcessManager: Boat server stop [" + \
                  "name=" + self.name + \
                  "]")
        else:
            print("ProcessManager: Process stop [" + \
                  "name=" + self.name + \
                  "]")

        # Send stop ipc message to the process
        if not self.clientConnection is None:
            self.clientConnection.send(["stop"])

        # Wait ok message from the process
        msg = self.serverConnection.recv()

        # Check message
        if msg[0] == "OK":
            # Close all connections
            self.listener.close()
            self.serverConnection.close()
            self.clientConnection.close()

            # None process and connections
            self.process = None
            self.listener = None
            self.serverConnection = None
            self.clientConnection = None

    def send(self, msg):
#        if self.serverMode:
#            print("ProcessManager: Boat server send [" + \
#                  "name=" + self.name + \
#                  ", msg=" + str(msg) + \
#                  "]")
#        else:
#            print("ProcessManager: Process send [" + \
#                  "name=" + self.name + \
#                  ", msg=" + str(msg) + \
#                  "]")

        try:
            # Check if the client connection is open
            if not self.clientConnection is None:
                # Send ipc message to the process
                self.clientConnection.send(msg)

        except:
            return False

        return True

    def receive(self):
        # Receive message from the process
        msg = self.serverConnection.recv()

#        if self.serverMode:
#            print("ProcessManager: Boat server receive [" + \
#                  "name=" + self.name + \
#                  "msg=" + str(msg) + \
#                  "]")
#        else:
#            print("ProcessManager: Process receive [" + \
#                  "name=" + self.name + \
#                  "msg=" + str(msg) + \
#                  "]")

        return msg

    def poll(self, timeout=2):
#        if self.serverMode:
#            print("ProcessManager: Boat server poll [" + \
#                  "name=" + self.name + \
#                  ", timeout=" + str(timeout) + \
#                  "]")
#        else:
#            print("ProcessManager: Process poll [" + \
#                  "name=" + self.name + \
#                  ", timeout=" + str(timeout) + \
#                  "]")

        # Poll message from the process for timeout second
        return self.serverConnection.poll(timeout)

    def close(self):
        print("ProcessManager: close [" + \
              "name=" + self.name + \
              "]")

        # Close all connections
        if not self.listener is None:
            self.listener.close()
        if not self.serverConnection is None:
            self.serverConnection.close()
        if not self.clientConnection is None:
            self.clientConnection.close()