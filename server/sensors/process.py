#!/usr/bin/env python
#-- coding: utf-8 --

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from processmanager import ProcessManager
from gpiozero import DigitalInputDevice, MCP3202

# Create a DigitalInput device instance for the water presence monitoring
waterPresencePIN = 24
waterPresenceActiveHigh = False
waterPresence = DigitalInputDevice(waterPresencePIN, \
                                   pull_up=None,
                                   active_state=waterPresenceActiveHigh,
                                   bounce_time=None,
                                   pin_factory=None)

# Create a Mcp3202 spi device instance for the motor temperature monitoring
motorTemperature = MCP3202(channel=0)

# Create a Mcp3202 spi device instance for the battery voltage monitoring
batteryVoltage = MCP3202(channel=1)

# Create the process manager in client mode to communicate with the boat server
processManager = ProcessManager("sensors", serverMode=False, clientPort=6005, serverPort=6004)

# Start the ipc communication with the boat server
processManager.start()

while True:
    try:
        # Wait message from boat server
        msg = processManager.receive()
        #print ("Sensors: Received message: %s" % msg)

        # Check if a get message
        if msg[0] == "get":
            # Check if a get status message
            if msg[1] == "status":
                #print ("Sensors: Received get status")
                try:
                    # Get battery voltage (1Kohm and 680ohm resistors voltage divider)
                    batteryVoltageValue = (batteryVoltage.value * 8.0)
                    # Get motor temperature (TTC682 and 6,8Kohm resistor divider)
                    motorTemperatureValue = (100.0 / (motorTemperature.value * 8.0))
                    if motorTemperatureValue > 150.0:
                        motorTemperatureValue = 150.0
                    # Get CPU temperature
                    cpuTempFile = open("/sys/class/thermal/thermal_zone0/temp","r")
                    cpuTemperature = float(cpuTempFile.read())/1000
                    cpuTempFile.close()

                    # Send battery voltage, motor and cpu temperature and water presence
                    processManager.send(["OK", \
                                         batteryVoltageValue, \
                                         motorTemperatureValue, \
                                         cpuTemperature, \
                                         waterPresence.value])
                except Exception, e:
                    # Send error
                    processManager.send(["KO - Exception: " + str(e)])

        # Check if a set message
        elif msg[0] == "set":
            print ("Sensors: Received setting [key=%s, value=%s]" % (msg[1], msg[2]))
            waterPresenceSetting = False
            # Check if the message is water presence pin setting
            if msg[1].endswith("water-presence-pin"):
                # Check new setting not equal to current setting
                if waterPresencePIN <> int(msg[2]):
                    print ("Sensors: Setting [key=%s, current_value=%d, new_value=%s]" % (msg[1], waterPresencePIN, msg[2]))
                    # Update the water presence pin
                    waterPresencePIN = int(msg[2])
                    waterPresenceSetting = True

            # Check if the message is water presence active high setting
            elif msg[1].endswith("water-presence-active-high"):
                # Check new setting not equal to current setting
                if waterPresenceActiveHigh <> ((msg[2] == "true") or (msg[2] == "True")):
                    print ("Sensors: Setting [key=%s, current_value=%d, new_value=%s]" % (msg[1], waterPresenceActiveHigh, msg[2]))
                    # Update the water presence reverse active high
                    waterPresenceActiveHigh = (msg[2] == "true") or (msg[2] == "True")
                    waterPresenceSetting = True

            # Check if a water presence setting => Need to restart the device to apply the settings
            if waterPresenceSetting:
                print ("Sensors: Restarting the water presence DigitalInputDevice")
                # Close the water presence gpio
                waterPresence.close()

                # Create a DigitalInput device instance for the water presence monitoring
                waterPresence = DigitalInputDevice(waterPresencePIN, \
                                                   pull_up=None,
                                                   active_state=waterPresenceActiveHigh,
                                                   bounce_time=None,
                                                   pin_factory=None)

        # Check if a stop message
        elif msg[0] == "stop":
            # Close the MCP3202 device
            MCP3202.close(batteryVoltage)
            MCP3202.close(motorTemperature)

            # Close the water presence device
            waterPresence.close()

            # Send OK message to the boat server
            processManager.send(["OK"])

            # Close all ipc connection
            processManager.close()

            # Break the main loop
            break

    except ValueError, e:
        print ("Sensors: Exception !!! message:" + str(e))

    except KeyboardInterrupt:
        print ("Sensors: KeyboardInterrupt !!!")
        # Close the Mcp3202 device
        MCP3202.close(batteryVoltage)
        MCP3202.close(motorTemperature)
        # Close the water presence device
        waterPresence.close()        
        break
