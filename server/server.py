#!/usr/bin/env python
#-- coding: utf-8 --

# requirement:
# sudo pip install flask

import os, sys
from flask import Flask
from processmanager import ProcessManager

# Initialize variables
app = Flask(__name__)

# Process manager dictionary
processManager = {"motor":ProcessManager("motor", clientPort=6000, serverPort=6001), \
                  "direction":ProcessManager("direction", clientPort=6002, serverPort=6003), \
                  "sensors":ProcessManager("sensors", clientPort=6004, serverPort=6005)}

@app.route("/")
def main():
    return "/boat/[start,stop]/[motor,direction,sensors,camera,all]<br>" + \
           "/boat/move/[speed--100-to-100]/[direction-0-to-180]<br>" + \
           "/boat/settings/[server,motor,direction,sensors]/[key]/[value]<br>" + \
           "/boat/status"

@app.route("/boat/move/<speed>/<direction>")
def boatMove(speed, direction):
    global processManager

    result = True
    #print ("Server: boatMove (speed = %s, direction = %s)" % (speed, direction))

    # Check if motor process is active
    if processManager["motor"].isActive():
        # Send new speed to the motor process
        if not processManager["motor"].send(["move", speed]):
            result = False
    else:
        result = False

    # Check if direction process is active
    if processManager["direction"].isActive():
        # Send new direction to the direction process
        if not processManager["direction"].send(["move", direction]):
            result = False
    else:
        result = False

    if result:
        result = "OK - request : speed = " + speed + " direction = " + direction
    else:
        result = "KO - request : speed = " + speed + " direction = " + direction
    return result

@app.route("/boat/status")
def boatStatus():
    global processManager

    try:
        # Check if sensors process is active
        if processManager["sensors"].isActive():
            # Send status request to the sensors process
            processManager["sensors"].send(["get", "status"])

            # Wait message from sensors process
            msg = processManager["sensors"].receive()
            #print ("Server: Received message: %s" % str(msg))

            # Check result
            if msg[0] == "OK":
                # Extract values from message
                batteryVoltage = str(msg[1])
                motorTemperature = str(msg[2])
                cpuTemperature = str(msg[3])
                waterPresence = str(msg[4])
                return "OK - status : " + \
                       "batteryVoltage = " + batteryVoltage + " ; " + \
                       "motorTemperature = " + motorTemperature + " ; " + \
                       "cpuTemperature = " + cpuTemperature + " ; " + \
                       "waterPresence = " + waterPresence + " ; " + \
                       "motor = " + str(processManager["motor"].isActive()) + " ; " + \
                       "direction = " + str(processManager["direction"].isActive()) + " ; " + \
                       "sensors = " + str(processManager["sensors"].isActive())

            else:
                return "KO - status : Sensors process return KO. Msg: " + msg[0]

        else:
            return "KO - status : not connected to sensors process"

    except Exception, e:
        return "KO - status : Exception: " + str(e)

@app.route("/boat/<action>/<part>")
def boatAction(action, part):
    global processManager

    #print ("Server: boatAction (action = %s, part = %s)" % (action, part))
    try:
        if action == "start":
            # Check part
            if part == "motor":
                # Check if motor process is not active
                if not processManager["motor"].isActive():
                    # Start the motor process
                    processManager["motor"].start()

            elif part == "direction":
                # Check if direction process is not active
                if not processManager["direction"].isActive():
                    # Start the direction process
                    processManager["direction"].start()

            elif part == "sensors":
                # Check if sensors process is not active
                if not processManager["sensors"].isActive():
                    # Start the sensors process
                    processManager["sensors"].start()

            elif part == "camera":
                # Start motion with systemctl
                os.system('sudo systemctl start motion')

            elif part == "all":
                # Check if motor process is not active
                if not processManager["motor"].isActive():
                    # Start the motor process
                    processManager["motor"].start()

                # Check if direction process is not active
                if not processManager["direction"].isActive():
                    # Start the direction process
                    processManager["direction"].start()

                # Check if sensors process is not active
                if not processManager["sensors"].isActive():
                    # Start the sensors process
                    processManager["sensors"].start()

                # Start motion with systemctl
                os.system('sudo systemctl start motion')

            # Set boat speed to 0% and direction to 90°
            boatMove("0", "90")

        elif action == "stop":
            # Set boat speed to 0% and direction to 90°
            boatMove("0", "90")

            # Check part
            if part == "motor":
                # Check if motor process is active
                if processManager["motor"].isActive():
                    # Stop the motor process
                    processManager["motor"].stop()

            elif part == "direction":
                # Check if direction process is active
                if processManager["direction"].isActive():
                    # Stop the direction process
                    processManager["direction"].stop()

            elif part == "sensors":
                # Check if sensors process is active
                if processManager["sensors"].isActive():
                    # Stop the sensors process
                    processManager["sensors"].stop()

            elif part == "camera":
                # Stop motion with systemctl
                os.system('sudo systemctl stop motion')

            elif part == "all":
                # Check if motor process is active
                if processManager["motor"].isActive():
                    # Stop the motor process
                    processManager["motor"].stop()

                # Check if direction process is active
                if processManager["direction"].isActive():
                    # Stop the direction process
                    processManager["direction"].stop()

                # Check if sensors process is active
                if processManager["sensors"].isActive():
                    # Stop the sensors process
                    processManager["sensors"].stop()

                # Stop motion with systemctl
                os.system('sudo systemctl stop motion')

        return "OK - " + part + " " + action

    except Exception, e:
        return "KO - Exception: " + str(e)

@app.route("/boat/settings/<part>/<key>/<value>")
def boatSettings(part, key, value):
    global processManager

    #print ("Server: boatSettings (part = %s, key = %s, value = %s)" % (part, key, value))
    # Check part
    if part == "server":
        # Check key
        if key == "":
            # TODO: Add settings if needed
            pass

    elif part == "motor":
        # Check if motor process is connected
        if processManager["motor"].isActive():
            # Send key and value to motor process
            processManager["motor"].send(["set", key, value])

    elif part == "direction":
        # Check if direction process is connected
        if processManager["direction"].isActive():
            # Send key and value to direction process
            processManager["direction"].send(["set", key, value])

    elif part == "sensors":
        # Check if sensors process is connected
        if processManager["sensors"].isActive():
            # Send key and value to sensors process
            processManager["sensors"].send(["set", key, value])

    return "OK - " + part + " " + key + " " + value

if __name__ == "__main__":
   app.run(host='0.0.0.0', port=5000, debug=True, threaded=True)
